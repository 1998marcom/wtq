#ifndef WTQ_COMMON
#define WTQ_COMMON

// Defines
#define FOR(i, N) for(int i=0; i<N; i++)
#define FORL(i, N) for(long long i=0; i<N; i++)
#define MAX(a, b) a > b ? a : b
#define MIN(a, b) a < b ? a : b
#define EPSILON 1e-25

#include <utility>
#include <memory>
#include <vector>
#include <complex>
#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/Dense>
#include <eigen3/unsupported/Eigen/MatrixFunctions>

// Forward declarations
namespace wtq {
	class QVariable;
	class QBit;

	typedef std::complex<double> cf;
	typedef Eigen::SparseVector<cf> SparseVectorXcf;
	typedef Eigen::SparseMatrix<cf, Eigen::RowMajor> SparseMatrixXcf;
	typedef Eigen::VectorXcd VectorXcf;
	typedef Eigen::VectorXd VectorXf;
	typedef Eigen::MatrixXcd MatrixXcf;
	typedef Eigen::MatrixXd MatrixXf;
	typedef long long int ll;
	PYTHON_PREPROCESSOR{{{output(MCWS("""
	typedef std::vector<std::pair<cf, std::vector<MatrixType>>> MatrixTypeCombination;
	"""))}}}
	enum class StateType { 
		sparse_state = 0, 
		dense_state, 
		sparse_density_matrix, 
		density_matrix 
	};

    class LocalState;
    class LocalStateSparseState;
    class LocalStateDenseState;
    class LocalStateDensityMatrix;
    class LocalStateTensorEnsemble;
    typedef std::vector<std::shared_ptr<LocalState>> TensorState;
    typedef struct {
        double probability;
        TensorState state;
    } TensorPair;
    class TensorEnsemble;
	class GlobalState;

	class Functor;
    class MultiFunctor;
    template <typename MatrixT> class ControlU;
    template <typename MatrixT> class StoneUnitary;
}

//Cannot typedef a template
#define Triplet Eigen::Triplet

/* Here we define qbit indexing using short, so don't expect the compiler to let
 * you work with more than 16k qbits. As of now, classical computers can
 * simulate few tens of qbits fully-entangled. There's space for hundreds of
 * these systems in a short, so we are indeed satisfied.
 */


//#include "containers/globalstate.hpp"

namespace wtq {

	class QBit {
	private:
		GlobalState* global_state_ptr;
		short bit_number;
	public:
		short get_address() const;
		double get_value() const;
		int measure(const int& force_result=-1) const;
		QBit(GlobalState* in_global_state_ptr, const short& in_bit_number);
		GlobalState* _get_global_state_ptr() const;
		template <typename T> const QBit& operator *= (const T& U) const;
		template <typename T> const QBit& reset(const T& data) const;
		StateType max_state_type() const;
	};
	
	class QVariable {
	private: 
		GlobalState* global_state_ptr;
		std::vector<short> bit_numbers;
	public:
		//std::vector<short> get_addresses() const;
		const std::vector<short>& get_addresses() const;
		Eigen::VectorXf get_values() const;
		std::vector<int> measure(const std::vector<int>& force_result=std::vector<int>()) const;
		ll measure_int(const ll& force_result=((ll)-1)) const;
		QVariable(GlobalState* in_global_state_ptr, const std::vector<short>& bit_numbers);
		//QVariable(const QBit& qbit);
		PYTHON_PREPROCESSOR{{{
			for i in range(10):
				outstr = "QVariable(const QBit& qbit0"
				for j in range(1,i+1):
					outstr += f", const QBit& qbit{j}"
				output(outstr+");")
			for i in range(10):
				outstr = "QVariable(const QVariable& qvar0"
				for j in range(1,i+1):
					outstr += f", const QVariable& qvar{j}"
				output(outstr+");")
		}}}
		int hilbert_dims() const;
		ll data_size() const;
		GlobalState* _get_global_state_ptr() const;
		QBit operator [] (const int& index) const;
		template <typename T> const QVariable& operator *= (const T& U) const;
		template <typename T> const QVariable& reset(const T& data) const;
		StateType max_state_type() const;
		PYTHON_PREPROCESSOR{{{output(MCWS("""
		const QVariable& apply_Us(const MatrixTypeCombination& mc) const;
		const QVariable& apply_exp(const MatrixType& H) const;
		"""))}}}
	};

	void swap(const QVariable& a, const QVariable& b);

	// The following are so general that they are difficult to explicitly
	// instanciate
	template<typename T1, typename T2> T1 int_pow(const T1& base, const T2& ex) {
		T1 result = 1;
		for(T2 i=ex; i>0; i--) {
			result *= base;
		}
		return result;
	}
	template<typename T1, typename T2> bool contains(T1 item, T2 container) {
		for(const auto& element: container) {
			if (item == element)
				return true;
		}
		return false;
	}

	template <typename T>
	T factorial(const T& i) {
		T result = 1;
		T tmp;
		for(tmp=i; tmp>1; tmp--) {
			result *= tmp;
		}
		return result;
	}
	template <typename T>
	T stirling_factorial(const T& i) {
		return sqrt(2*std::numbers::pi_v<double>*i)*std::pow(i/std::numbers::e_v<double>,i);
	}
	
	template<typename MatrixT>
	double estimate_lambda_max(const MatrixT& mat);

}

#endif
