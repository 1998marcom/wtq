#ifndef WTQ_CONTAINERS_GLOBALSTATE
#define WTQ_CONTAINERS_GLOBALSTATE

#include "containers/localstate.hpp"

namespace wtq {
	class GlobalState : public LocalStateTensorEnsemble {
	protected:
	public:
		GlobalState(); 
		//~GlobalState(); // not needed thanks to smart pointers
		QVariable add_variable(const int& bit_width);
		//GlobalState full_copy() const;
		int measure(const short& address, const int& force_result=-1);
		PYTHON_PREPROCESSOR{{{output(MCWS("""
		void apply_Us(const QVariable& qvar, const MatrixTypeCombination& mc);
		void apply_exp(const QVariable& qvar, const MatrixType& H);
		"""))}}}
	};
}

#endif
