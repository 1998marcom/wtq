#ifndef WTQ_CONTAINERS_LOCALSTATE
#define WTQ_CONTAINERS_LOCALSTATE

#include <memory>
#include <complex>
#include <vector>
#include <utility>
#include <string>

#include "common.hpp"

namespace wtq {
	class LocalState {
	/*Note that this is an abstract class with no real data storage
	 * capabilities
	 */
	protected:
		GlobalState* global_state_ptr;
		std::vector<short> own_bit_addresses;
	public:
		virtual std::string class_type();
		LocalState() {}
		LocalState(GlobalState* in_global_state_ptr, const std::vector<short>& own_bit_addresses);
		LocalState(const LocalState& ls, const std::vector<short>& own_bit_addresses);
		PYTHON_PREPROCESSOR{{{output(MCWS(
		"""
		template<typename MatrixT> MatrixT get_complete_U(const QVariable& input, const MatrixType& U);
		virtual void apply_U(const QVariable& input, const MatrixType& U) = 0;
		//virtual void apply_cU(const QBit& control_bit, const QVariable& input, const MatrixType& U) = 0;
		virtual void apply_lUs(const QVariable& qvar, const MatrixTypeCombination& mc) {}
		virtual void apply_rUs(const QVariable& qvar, const MatrixTypeCombination& mc) {}
		virtual void apply_lexp(const QVariable& qvar, const MatrixType& H) {}
		virtual void apply_rexp(const QVariable& qvar, const MatrixType& H) {}
		"""
		))}}}
		std::vector<short> get_own_bit_addresses() const;
		GlobalState* _get_global_state_ptr() const;
		virtual double get_value(const short& address) const = 0;
		//Eigen::VectorXf get_values() const;
		void check_collapse_possibility(const short& address, const int& value) const;
		virtual void collapse(const short& address, const int& value) = 0;
		ll address_to_index_addendum(
			const short& address, 
			const std::vector<short>& bit_addrs=std::vector<short>()
		) const;
		PYTHON_PREPROCESSOR{{{output(MCWS(
		"""
		virtual void smart_reset(const QVariable& input, const DataType& data) {};
		"""
		))}}}
		//virtual vector<LocalState*> _return_minimal_states(const QVariable& input) = 0;
		virtual void flatten() {}
	};

	class LocalStateSparseState : public LocalState {
	protected:
		SparseVectorXcf data;
	public:
		SparseVectorXcf& _get_data();
		LocalStateSparseState(
			GlobalState* in_global_state_ptr, 
			const std::vector<short>& own_bit_addresses,
			const SparseVectorXcf& in_data
		);
		const SparseVectorXcf& _get_const_data() const;
		std::string class_type() override;
		using LocalState::LocalState;
		LocalStateSparseState(const LocalStateSparseState& ls1, const LocalStateSparseState& ls2);
		PYTHON_PREPROCESSOR{{{output(MCWS(
		"""
		void apply_U(const MatrixType& U);
		void apply_U(const QVariable& input, const MatrixType& U) override;
		void apply_lUs(const QVariable& qvar, const MatrixTypeCombination& mc) override;
		void apply_lexp(const QVariable& qvar, const MatrixType& H) override;
		//void apply_cU(const QBit& control_bit, const QVariable& input, const MatrixType& U) override;
		"""
		))}}}
		virtual double get_value(const short& address) const override;
		virtual void collapse(const short& address, const int& value) override;
		void reset(const QVariable& input, SparseVectorXcf data);
	}; 

	class LocalStateDenseState : public LocalState {
	protected:
		VectorXcf data;
	public:
		VectorXcf& _get_data();
		const VectorXcf& _get_data() const;
		const VectorXcf& _get_const_data() const;
		std::string class_type() override;
		using LocalState::LocalState;
		LocalStateDenseState(
			GlobalState* in_global_state_ptr, 
			const std::vector<short>& own_bit_addresses,
			const VectorXcf& in_data
		);
		PYTHON_PREPROCESSOR{{{output(MCWS(
		"""
		void apply_U(const MatrixType& U);
		void apply_U(const QVariable& input, const MatrixType& U) override;
		void apply_lUs(const QVariable& qvar, const MatrixTypeCombination& mc) override;
		//void apply_cU(const QBit& control_bit, const QVariable& input, const MatrixType& U) override;
		"""
		))}}}
		LocalStateDenseState(const LocalStateSparseState& input);
		LocalStateDenseState(const LocalStateDenseState& ls1, const LocalStateDenseState& ls2);
		virtual double get_value(const short& address) const override;
		virtual void collapse(const short& address, const int& value) override;
		void reset(const QBit& input, VectorXcf data);
		void reset(const QVariable& input, VectorXcf data);
	};

	class LocalStateDensityMatrix : public LocalState {
	protected:
		MatrixXcf data;
	public:
		MatrixXcf& _get_data();
		const MatrixXcf& _get_const_data() const;
		std::string class_type() override;
		using LocalState::LocalState;
		PYTHON_PREPROCESSOR{{{output(MCWS(
		"""
		//void apply_U(const MatrixType& U);
		void apply_U(const QVariable& input, const MatrixType& U) override;
		//void apply_cU(const QBit& control_bit, const QVariable& input, const MatrixType& U) override;
		void apply_lUs(const QVariable& qvar, const MatrixTypeCombination& mc) override;
		void apply_rUs(const QVariable& qvar, const MatrixTypeCombination& mc) override;
		"""
		))}}}
		LocalStateDensityMatrix(const LocalStateSparseState& input);
		LocalStateDensityMatrix(const LocalStateDenseState& input);
		LocalStateDensityMatrix(const LocalStateTensorEnsemble& input);
		LocalStateDensityMatrix(const LocalStateDensityMatrix& ls1, const LocalStateDensityMatrix& ls2);
		virtual double get_value(const short& address) const override;
		virtual void collapse(const short& address, const int& value) override;
		void reset(const QVariable& input, MatrixXcf data);
	};

	class TensorEnsemble : public std::vector<TensorPair> {
	public:
		using std::vector<TensorPair>::vector;
		TensorEnsemble(TensorState& almost_flat_ts);
		TensorEnsemble flatten() const;
		TensorEnsemble& prune_and_normalize();
		TensorEnsemble& collapse(const short& address, const int& value);
		PYTHON_PREPROCESSOR{{{output(MCWS(
		"""
		TensorEnsemble tensorize(AllLocalStateType& ls) const;
		"""
		))}}}
	};

	class LocalStateTensorEnsemble : public LocalState {
	protected:
		TensorEnsemble data;
	public:
		TensorEnsemble& _get_data();
		const TensorEnsemble& _get_const_data() const;
		std::string class_type() override;
		using LocalState::LocalState;
		PYTHON_PREPROCESSOR{{{output(MCWS(
		"""
		void apply_U(const QVariable& input, const MatrixType& U) override;
		//void apply_cU(const QBit& control_bit, const QVariable& input, const MatrixType& U) override;
		void apply_lUs(const QVariable& qvar, const MatrixTypeCombination& mc) override;
		void apply_rUs(const QVariable& qvar, const MatrixTypeCombination& mc) override;
		void apply_lexp(const QVariable& qvar, const MatrixType& H) override;
		void apply_rexp(const QVariable& qvar, const MatrixType& H) override;
		"""
		))}}}
		virtual double get_value(const short& address) const override;
		virtual void collapse(const short& address, const int& value) override;
		PYTHON_PREPROCESSOR{{{output(MCWS("""
		virtual void smart_reset(const QVariable& input, const DataType& new_data) override;
		"""))}}}
		void flatten() override;
		StateType max_state_type(const QBit& qbit) const;
	};

	TensorState find_involved_sublocal_states(const QVariable& input, const TensorState& ts);

	/** magic_merge: returns a tensor state with only one element of the
	 * appropriate LocalState-derived class, depending on argument's data types
	 */
	TensorState magic_merge(TensorState& ts); 
	TensorEnsemble operator * (const TensorEnsemble& te, const double& alpha);
	TensorEnsemble operator * (const double& alpha, const TensorEnsemble& te); 

	PYTHON_PREPROCESSOR{{{output(MCWS("""
	std::ostream& operator << (std::ostream& os, FlatLocalStateType& ls);
	"""))}}}
	std::ostream& operator << (std::ostream& os, LocalStateTensorEnsemble& ls);
	std::ostream& operator << (std::ostream& os, TensorEnsemble& te);
}

#endif
