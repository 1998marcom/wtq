#include "common.hpp"

namespace wtq {

	MatrixXcf get_qft(const int& dimensions, const int& exponent_sign=1);

	void qft(const QVariable& input, const int& exponent_sign=1);

}
