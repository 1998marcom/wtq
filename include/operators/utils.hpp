#ifndef WTQ_OPERATORS_UTILS
#define WTQ_OPERATORS_UTILS

#include "common.hpp"
#include <iostream>

namespace wtq {

	template <typename MatrixT>
	const QVariable& operator*= (const QVariable& qvar, StoneUnitary<MatrixT>& su);

	class Functor {
	protected:
		int dimensions;
	public:
		Functor(const int& dimensions);
		virtual ll operator() (const ll& in) = 0;
		SparseMatrixXcf to_hilbert_matrix();
	};

	class MultiFunctor {
	protected:
		int dimensions;
	public:
		MultiFunctor(const int& dimensions);
		virtual SparseVectorXcf operator() (const ll& in) = 0;
		SparseMatrixXcf to_hilbert_matrix(const ll& inverse_verbosity = 0);
	};

	template <typename MatrixT>
	class ControlU {
	private:
		MatrixT cu;
	public:
		ControlU(const MatrixT& U);
		void operator() (const QBit& control, const QVariable& input, const int& direction=1) const;
		const MatrixT get_matrix() const;
	};

	template <typename MatrixT>
	class ControlH {
	private:
		MatrixT ch;
	public:
		ControlH(const MatrixT& H);
		void operator() (const QBit& control, const QVariable& input, const int& direction=1) const;
		const MatrixT get_matrix() const;
	};

	template <typename MatrixT>
	class StoneUnitary {
	private:
		MatrixT herm;
		MatrixT unitary;
		bool high_precision;
	public:
		StoneUnitary(const MatrixT& herm, const bool& high_precision=false);
		friend const QVariable& operator*= <MatrixT> (const QVariable& qvar, StoneUnitary<MatrixT>& su);
		bool is_get_matrix_convenient() const;
		const MatrixT get_matrix();
	};

	template <typename MatrixT>
	const QVariable& operator*= (const QVariable& qvar, StoneUnitary<MatrixT>& su);
		
	
	/** TO TRASH
	std::ostream& operator << (std::ostream& os, const SparseMatrixXcf& M);
	std::ostream& operator << (std::ostream& os, const MatrixXcf& M);
	*/

}

#endif
