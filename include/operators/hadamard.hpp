#include "common.hpp"

namespace wtq {
	MatrixXcf get_hadamard();

	static MatrixXcf hadamard = get_hadamard();
}
