#include "common.hpp"

namespace wtq {
	enum class Pauli { Id=0, X, Y, Z };
	MatrixXcf get_pauli(const Pauli& key);

	PYTHON_PREPROCESSOR{{{output("\n".join([
	f"static MatrixXcf Pauli{X} = get_pauli(Pauli::{X});" for X in ["Id", "X", "Y", "Z"]
	]))}}}
}
