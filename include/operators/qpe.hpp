#include "common.hpp"
#include "operators/utils.hpp"
#include <vector>

namespace wtq {
	
	template <typename MatrixT>
	class QPE {
	private:
		MatrixT U;
		std::vector<ControlU<MatrixT>> cU2j;
	public:
		QPE(const MatrixT& U, const int& estimation_bits=0);
		void operator() (const QVariable& phi, const QVariable& E, const int& direction=1) const;
	};

	template <typename MatrixT>
	class QPE_H {
	private:
		MatrixT cH;
		//std::vector<ControlU<MatrixT>> cU2j;
		std::vector<StoneUnitary<MatrixT>> positive_unitaries;
		std::vector<StoneUnitary<MatrixT>> negative_unitaries;
	public:
		QPE_H(const MatrixT& H);
		void operator() (const QVariable& phi, const QVariable& E, const int& direction=1);
	};

}
