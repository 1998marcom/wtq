#include "common.hpp"
#include "containers/localstate.hpp"
#include "containers/globalstate.hpp"
#include "operators/qpe.hpp"
#include <complex>
#include <cmath>
#include <numbers>
#include <vector>
#include <iostream>
#include <bitset>
#include <chrono>
#include <set>
#include <random>
#include <fstream>
#include <sstream>

using namespace std;
using namespace std::chrono;

wtq::MatrixXcf stone_unitary(const wtq::MatrixXcf& herm, const float& t) {
	return (herm*t*wtq::cf(0,1)).exp();
}
wtq::MatrixXcf random_unitary(const long long& size) {
	wtq::MatrixXcf result(size, size);
	result.setRandom();
	result = (wtq::cf(0,2)*result*result.adjoint()).exp();
	return result;
}

wtq::SparseMatrixXcf random_hermitian_sparse(wtq::ll size, double p) {
	 //* Adapted from
	 //* https://stackoverflow.com/questions/30741884/sparse-random-matrix-with-eigen
	 //*
	wtq::SparseMatrixXcf result(size, size);
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> realvaldis(0, 0.3);
    std::uniform_real_distribution<> imagvaldis(0, 0.3);
    std::uniform_int_distribution<> rowdis(0, size-1);
    std::uniform_int_distribution<> coldis(0, size-1);

    std::vector<Triplet<wtq::cf> > tripletList;
    size_t nnz = (size_t) (size * (size * p));
    std::set<size_t> nnz_pos;
    for (size_t i = 0; i < nnz; ++i) {
        auto r = rowdis(gen);
        auto c = coldis(gen);
        size_t pos = r * size + c;
        while (nnz_pos.find(pos) != nnz_pos.end()) {
            r = rowdis(gen);
            c = coldis(gen);
            pos = r * size + c;
        }

        nnz_pos.insert(pos);
        tripletList.push_back(
			Triplet<wtq::cf>(r, c, wtq::cf(realvaldis(gen), imagvaldis(gen)))
		);
    }

	wtq::SparseMatrixXcf mat(size,size);
    mat.setFromTriplets(tripletList.begin(), tripletList.end());   //create the matrix
    return mat*mat.adjoint();
}

/** AccPseudoFunctor is a class whose instances can be applied to (Eold, Enew, acc)
 * to compute the acceprance acc as per the MCMC probabilities dictate.
 */
class AccPseudoFunctor {
	float t, beta;
	int Edims = 0;
	int dimensions = 0;
	wtq::SparseMatrixXcf matrix;
public:
	AccPseudoFunctor (const float& t, const float& beta=1.0){
		this->beta = beta;
		this->t = t;
	}
	void operator() (const wtq::QVariable& Eold, const wtq::QVariable& Enew, const wtq::QVariable& acc) {
		if (Eold.hilbert_dims() != Enew.hilbert_dims())
			throw std::runtime_error("Eold and Enew variables must have same size");
		if (dimensions == 0) {
			Edims = Eold.hilbert_dims();
			dimensions = Eold.hilbert_dims() + Enew.hilbert_dims() + 1;
			matrix = acc_matrix();
		}
		wtq::QVariable(Eold, Enew, acc) *= matrix;
	}
	wtq::SparseMatrixXcf acc_matrix() {
		wtq::ll N = ((wtq::ll) 1) << dimensions;
		wtq::SparseMatrixXcf result(N, N);
        FORL(i, N) {
			if (i%2) continue;
			wtq::ll Mnew_bitmask = (((wtq::ll) 1) << (Edims+1)) - 2;
			wtq::ll Mold_bitmask = Mnew_bitmask << Edims;
			float Eold = 2*numbers::pi/(1<<Edims)/t*(Mold_bitmask & i);
			float Enew = 2*numbers::pi/(1<<Edims)/t*(Mnew_bitmask & i);
			if (Enew <= Eold) {
				result.coeffRef(i+1, i) = wtq::cf(1,0);
			}
			else {
				float tp = exp(-beta*(Enew-Eold));
				result.coeffRef(i+1, i) = wtq::cf(sqrt(tp),0);
				result.coeffRef(i, i) = wtq::cf(sqrt(1-tp),0);
			}
        }
        return result;
    }

};

/** HamiltonianFunctor(dims).to_hilbert_matrix() is the hamiltonian. Most general
 * way to obtain it. See Functor::operator()(const ll&) and
 * MultiFunctor::operator()(const ll&) to learn about some easy ways to define
 * sparse operators, such as those used in the majority of local theories,
 * especially useful when using less "dense" states.
 */
class HamiltonianFunctor : public wtq::MultiFunctor {
public:
	using wtq::MultiFunctor::MultiFunctor;
	std::tuple<int,int,int> qbit_to_coordinates(const int& changed_qbit) {
		int X = 2, Y = 2, Z = 2;
		int x = changed_qbit % X;
		int y = (changed_qbit / X) % Y;
		int z = (changed_qbit / (X*Y)) % Z;
		return std::tuple<int,int,int>(x,y,z);
	}
	bool test_reachable(const wtq::ll& to, const wtq::ll& in) {
		wtq::ll diff = to ^ in;
		vector<int> changed_qbits;
		for (int current_bit=dimensions-1; current_bit >= 0; current_bit--) {
			if (diff % 2)
				changed_qbits.push_back(current_bit);
			diff >> 1;
		}
		if (changed_qbits.size() != 2) return false;
		auto [Xa, Ya, Za] = qbit_to_coordinates(changed_qbits[0]);
		auto [Xb, Yb, Zb] = qbit_to_coordinates(changed_qbits[1]);
		if (Za != Zb) return false;
		if (Xa == Xb and abs(Ya-Yb) == 1 or Ya == Yb and abs(Xb-Xa) == 1) return true;
		return false;
	}
	virtual wtq::SparseVectorXcf operator() (const wtq::ll& in) override {
		wtq::SparseVectorXcf result(((wtq::ll) 1) << dimensions);
		auto X = 2, Y = 2, Z = 2;
		auto down_lb = 0, down_ub = X*Y-1, up_lb = X*Y, up_ub = X*Y*2;
		int parity_in = 1;
		for (wtq::ll tmp=in; tmp>0; tmp /= 2) {
			if (tmp%2) parity_in *= -1;
		}
		FORL(i, ((wtq::ll) 1) << dimensions) {
			if (test_reachable(i, in)) {
				int parity_i = 1;
				for (wtq::ll tmp=i; tmp>0; tmp /= 2) {
					if (tmp%2) parity_i *= -1;
				}
				int parity = parity_i * parity_in;
				result.coeffRef(i) = wtq::cf(-1*parity,0);
			}
		}
		wtq::ll down_bitmask = (((wtq::ll) 1) << dimensions/2) - 1;
		wtq::ll up_bitmask = down_bitmask << dimensions/2;
		// in = 0111-1110 --> potential_preterm = 0111 & 1110 = 0110
		wtq::ll potential_preterm = (down_bitmask & in) & ((up_bitmask & in) >> dimensions/2);
		wtq::ll potential_term = 0;
		while (potential_preterm > 0) {
			if (potential_preterm % 2) {
				potential_term++;
			}
			potential_preterm = potential_preterm >> 1;
		}
		result.coeffRef(in) = wtq::cf(potential_term+12, 0);
		return result;
	}
};

int main() {

	// GlobalState instantiation
	wtq::GlobalState global_state;

	// Variable instantiation (might be entangled)
	wtq::QVariable phi = global_state.add_variable(8); // Add a variable with 2 qubits
	wtq::QVariable Eold = global_state.add_variable(5);
	wtq::QVariable Enew = global_state.add_variable(5);
	wtq::QVariable acc = global_state.add_variable(1); // ancilla for MC acceptance
	//cout << global_state;

	// Variable initialization
	wtq::SparseVectorXcf initial_phi(phi.data_size());
	initial_phi.coeffRef(0) = wtq::cf(1,0); // initial_phi is an hamiltonian eigenstate
	phi.reset(initial_phi);

	wtq::SparseVectorXcf initial_E(Eold.data_size());
	initial_E.coeffRef(0) = wtq::cf(1,0);

	wtq::SparseVectorXcf initial_acc(acc.data_size());
	initial_acc.coeffRef(0) = wtq::cf(1,0);

	//Eold.reset(wtq::SparseVectorXcf(Eold.data_size()));
	//Enew.reset(wtq::SparseVectorXcf(Enew.data_size()));
	//acc.reset(wtq::SparseVectorXcf(acc.data_size()));
	cout << "Initial state:\n" << global_state;

	// Define hamiltonian
	wtq::SparseMatrixXcf H = HamiltonianFunctor(phi.hilbert_dims()).to_hilbert_matrix();
	//cout << "Printing Hamiltonian:\n" << H << endl;

	// QMS algorithm
	//float t = 2*3.1416/Eold.hilbert_dims()*1.0;
	float t = 1.0/Eold.hilbert_dims()/2;
	wtq::SparseMatrixXcf adjustedH = t * H;
	auto qpe = wtq::QPE_H(adjustedH);
	wtq::ll N = 3; // how many runs
	wtq::ll M = 1000; // how many steps for each run
	auto acc_functor = AccPseudoFunctor(t, 1); // TODO
	double energy = 0.0;
	FOR(i, N) {
		auto start = high_resolution_clock::now();
		// Pure eigenstate initialization
		phi.measure(); phi.reset(initial_phi);
		stringstream oname;
		oname << "data/test_010_run_" << i << ".data";
		ofstream ofile(oname.str());

		cout << "Point 2, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		auto randH = random_hermitian_sparse(phi.data_size(), 2.4/phi.data_size());
		auto randU = wtq::StoneUnitary(randH);
		wtq::SparseMatrixXcf randHinverse = -1.0 * randH;
		auto randUinverse = wtq::StoneUnitary(randHinverse);
		cout << "Point 3, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;

		FOR(j, M) {
			cout << "Beginning step " << j << "/" << M << endl;
			// Registers reset
			Eold.measure(); Eold.reset(initial_E);
			Enew.measure(); Enew.reset(initial_E);
			acc.measure(); acc.reset(initial_acc);
		cout << "\tPoint 4, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
			// Eold
			qpe(phi, Eold);
		cout << "\tPoint 5, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
			energy += ((double) Eold.measure_int()) / (N*M);
			ofile << Eold.measure_int() << endl;
			// Random unitary
			phi *= randU;
		cout << "\tPoint 6, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
			// Enew
			qpe(phi, Enew);
		cout << "\tPoint 7, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
			// acc
			acc_functor(Eold, Enew, acc);
		cout << "\tPoint 8, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
			auto acc_measure = acc[0].measure();
		cout << "\tPoint 9, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
			Eold.measure(); // Factoring out Eold speeds up subsequent computations
		cout << "\tPoint a, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
			if (acc_measure == 1) {
				// Accept
				cout << "Enew = " << Enew.measure_int() << endl;
				cout << "ACCEPT\n";
		cout << "\tPoint b, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
			}
			else {
				// Reject
				cout << "REJECT\n";

				// acc_functor is defined so that if acc_measure == 0 there's no
				// need for an inverse transform

				// Inverse Enew QPE
				qpe(phi, Enew, -1);
		cout << "\tPoint c, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
				cout << "R\n";

				// Now Enew = 0
				Enew.measure();
		cout << "\tPoint d, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;

				// Inverse randU
				phi *= randUinverse;
		cout << "\tPoint e, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;

				
			}
			
			// If j big enough, do your measurements that commute with H here
			
		}
		ofile.close();

		// End of run, do your measurements that do not commute with H here
		cout << "Average energy during run: " << energy << endl;

	}

	// A lot of output, but at last it worked
	
	return 0;
}
