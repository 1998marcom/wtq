#include "common.hpp"
#include "containers/localstate.hpp"
#include "containers/globalstate.hpp"
#include "operators/qpe.hpp"
#include <complex>
#include <cmath>
#include <numbers>
#include <vector>
#include <iostream>

using namespace std;

wtq::MatrixXcf stone_unitary(const wtq::MatrixXcf& herm, const float& t) {
	return (herm*t*wtq::cf(0,1)).exp();
}
wtq::MatrixXcf random_unitary(const long long& size) {
	wtq::MatrixXcf result(size, size);
	result.setRandom();
	result = (wtq::cf(0,0.1)*result*result.adjoint()).exp();
//result = cf(1,0)*+cf(0,1)*wtq::MatrixXf::Ran
	return result;
}

int main() {

	// GlobalState instantiation
	wtq::GlobalState global_state;

	// Variable instantiation (might be entangled)
	wtq::QVariable phi = global_state.add_variable(2); // Add a variable with 2 qubits
	wtq::QVariable Eold = global_state.add_variable(3);
	wtq::QVariable Enew = global_state.add_variable(3);
	wtq::QVariable acc = global_state.add_variable(1); // ancilla for MC acceptance
	//cout << global_state;

	// Variable initialization
	wtq::SparseVectorXcf initial_phi(phi.data_size());
	initial_phi.coeffRef(0) = wtq::cf(1,0); // initial_phi is an hamiltonian eigenstate
	phi.reset(initial_phi);

	//Eold.reset(wtq::SparseVectorXcf(Eold.data_size()));
	//Enew.reset(wtq::SparseVectorXcf(Enew.data_size()));
	//acc.reset(wtq::SparseVectorXcf(acc.data_size()));
	cout << "Initial state:\n" << global_state;

	// Define hamiltonian
	wtq::SparseMatrixXcf H(4,4);
	H.coeffRef(0,0) = wtq::cf(1,0);
	H.coeffRef(1,1) = wtq::cf(2,0); H.coeffRef(2,2) = wtq::cf(2,0);
	H.coeffRef(1,2) = wtq::cf(2,0); H.coeffRef(2,1) = wtq::cf(2,0);
	H.coeffRef(3,3) = wtq::cf(5,0);
	cout << "Printing Hamiltonian:\n" << H << endl;

	// Operation QFT
	float t = 2*3.1416/6;
	auto qpe = wtq::QPE(stone_unitary(H, t), 3);
	qpe(phi, Eold);
	cout << global_state;
	double e = Eold.measure_int()*(2*std::numbers::pi/8)/t;
	cout << "Measured energy: " << e / t << endl;

	return 0;
}
