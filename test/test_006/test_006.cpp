#include "common.hpp"
#include "containers/localstate.hpp"
#include "containers/globalstate.hpp"
#include "operators/qpe.hpp"
#include <complex>
#include <cmath>
#include <numbers>
#include <vector>
#include <iostream>

using namespace std;

wtq::MatrixXcf stone_unitary(const wtq::MatrixXcf& herm, const float& t) {
	return (herm*t*wtq::cf(0,1)).exp();
}
wtq::MatrixXcf random_unitary(const long long& size) {
	wtq::MatrixXcf result(size, size);
	result.setRandom();
	result = (wtq::cf(0,2)*result*result.adjoint()).exp();
	return result;
}

/** AccPseudoFunctor is a class whose instances can be applied to (Eold, Enew, acc)
 * to compute the acceprance acc as per the MCMC probabilities dictate.
 */
class AccPseudoFunctor {
	float t, beta;
	int Edims = 0;
	int dimensions = 0;
	wtq::SparseMatrixXcf matrix;
public:
	AccPseudoFunctor (const float& t, const float& beta=1.0){
		this->beta = beta;
		this->t = t;
	}
	void operator() (const wtq::QVariable& Eold, const wtq::QVariable& Enew, const wtq::QVariable& acc) {
		if (Eold.hilbert_dims() != Enew.hilbert_dims())
			throw std::runtime_error("Eold and Enew variables must have same size");
		if (dimensions == 0) {
			Edims = Eold.hilbert_dims();
			dimensions = Eold.hilbert_dims() + Enew.hilbert_dims() + 1;
			matrix = acc_matrix();
		}
		wtq::QVariable(Eold, Enew, acc) *= matrix;
	}
	wtq::SparseMatrixXcf acc_matrix() {
		wtq::ll N = ((wtq::ll) 1) << dimensions;
		wtq::SparseMatrixXcf result(N, N);
        FORL(i, N) {
			if (i%2) continue;
			wtq::ll Mnew_bitmask = (((wtq::ll) 1) << (Edims+1)) - 2;
			wtq::ll Mold_bitmask = Mnew_bitmask << Edims;
			float Eold = 2*numbers::pi/2/t*(Mold_bitmask & i);
			float Enew = 2*numbers::pi/2/t*(Mnew_bitmask & i);
			if (Enew <= Eold) {
				result.coeffRef(i+1, i) = wtq::cf(1,0);
			}
			else {
				float tp = exp(-beta*(Enew-Eold));
				result.coeffRef(i+1, i) = wtq::cf(sqrt(tp),0);
				result.coeffRef(i, i) = wtq::cf(sqrt(1-tp),0);
			}
        }
        return result;
    }

};

/** HamiltonianFunctor(dims).to_hilbert_matrix() is the hamiltonian. Most general
 * way to obtain it. See Functor::operator()(const ll&) and
 * MultiFunctor::operator()(const ll&) to learn about some easy ways to define
 * sparse operators, such as those used in the majority of local theories,
 * especially when using less "dense" states.
 */
class HamiltonianFunctor : public wtq::MultiFunctor {
public:
	using wtq::MultiFunctor::MultiFunctor;
	virtual wtq::SparseVectorXcf operator() (const wtq::ll& in) override {
		wtq::SparseVectorXcf result(((wtq::ll) 1) << dimensions);
		result.coeffRef(in) = wtq::cf(0.25,0);
		FOR(i, dimensions) {
			for(auto j = i+1; j<dimensions; j++) {
				wtq::ll flip_bitmask = 0;
				flip_bitmask += 1 << i;
				flip_bitmask += 1 << j;
				wtq::ll flipped_state = flip_bitmask ^ in;
				result.coeffRef(flipped_state) = wtq::cf(0.25, 0);
			}
		}
		return result;
	}
};

int main() {

	// GlobalState instantiation
	wtq::GlobalState global_state;

	// Variable instantiation (might be entangled)
	wtq::QVariable phi = global_state.add_variable(3); // Add a variable with 2 qubits
	wtq::QVariable Eold = global_state.add_variable(1);
	wtq::QVariable Enew = global_state.add_variable(1);
	wtq::QVariable acc = global_state.add_variable(1); // ancilla for MC acceptance
	//cout << global_state;

	// Variable initialization
	wtq::SparseVectorXcf initial_phi(phi.data_size());
	initial_phi.coeffRef(0) = wtq::cf(1,0); // initial_phi is an hamiltonian eigenstate
	phi.reset(initial_phi);

	wtq::SparseVectorXcf initial_E(Eold.data_size());
	initial_E.coeffRef(0) = wtq::cf(1,0);

	wtq::SparseVectorXcf initial_acc(acc.data_size());
	initial_acc.coeffRef(0) = wtq::cf(1,0);

	//Eold.reset(wtq::SparseVectorXcf(Eold.data_size()));
	//Enew.reset(wtq::SparseVectorXcf(Enew.data_size()));
	//acc.reset(wtq::SparseVectorXcf(acc.data_size()));
	cout << "Initial state:\n" << global_state;

	// Define hamiltonian
	wtq::SparseMatrixXcf H = HamiltonianFunctor(phi.hilbert_dims()).to_hilbert_matrix();
	cout << "Printing Hamiltonian:\n" << H << endl;

	// QMS algorithm
	float t = 2*3.1416/2;
	auto qpe = wtq::QPE(stone_unitary(H, t), Eold.hilbert_dims());
	wtq::ll N = 1; // how many runs
	wtq::ll M = 1000; // how many steps for each run
	auto acc_functor = AccPseudoFunctor(t, 1); // TODO
	double energy = 0.0;
	FOR(i, N) {
		// Pure eigenstate initialization
		phi.measure(); phi.reset(initial_phi);

		FOR(j, M) {
			// Registers reset
			Eold.measure(); Eold.reset(initial_E);
			Enew.measure(); Enew.reset(initial_E);
			acc.measure(); acc.reset(initial_acc);
			// Eold
			qpe(phi, Eold);
			energy += ((double) Eold.measure_int()) / (N*M);
			// Random unitary
			auto randU = random_unitary(phi.data_size());
			phi *= randU;
			// Enew
			qpe(phi, Enew);
			// acc
			acc_functor(Eold, Enew, acc);
			auto acc_measure = acc[0].measure();
			if (acc_measure == 1) {
				// Accept
				cout << "Enew = " << Enew.measure_int() << endl;
				Eold.measure();
				cout << "ACCEPT\n";
			}
			else {
				// Reject
				cout << "REJECT\n";

				// acc_functor is defined so that if acc_measure == 0 there's no
				// need for an inverse transform

				// Inverse Enew QPE
				qpe(phi, Enew, -1);

				// Inverse randU
				wtq::MatrixXcf randUinverse = randU.adjoint();
				phi *= randUinverse;

				
			}
			
			// If j big enough, do your measurements that commute with H here
			
		}

		// End of run, do your measurements that do not commute with H here
		cout << "Average energy during run: " << energy << endl;

	}

	// A lot of output, but at last it worked
	
	return 0;
}
