#include "common.hpp"
#include "containers/localstate.hpp"
#include "containers/globalstate.hpp"
#include "operators/qft.hpp"
#include <complex>
#include <cmath>
#include <vector>
#include <iostream>

using namespace std;

int main() {

	// GlobalState instantiation
	wtq::GlobalState global_state;

	// Variable instantiation (might be entangled)
	wtq::QVariable A = global_state.add_variable(2); // Add a variable with 2 qubits
	//cout << global_state;

	// Variable initialization
	wtq::SparseVectorXcf A_initial_state(4);
	A_initial_state.coeffRef(2) = complex<float>(1.0/sqrt(2.0),0.0); // <10|A>
	A_initial_state.coeffRef(3) = complex<float>(1.0/sqrt(2.0),0.0); // <11|A>
	A.reset(A_initial_state);
	//cout << global_state;

	// Operation QFT
	A *= wtq::get_qft(2); 
	cout << "Printing global state after having applied qft on |10> + |11> \n"
		 << global_state;

	// Inverse QFT
	wtq::qft(A, -1);
	cout << "Printing global state after having applied inverse qft \n"
		 << global_state;

	auto N = 30000;
	FOR(i, N) {
		wtq::qft(A, 1);
		wtq::qft(A, -1);
	}
	cout << "Printing global state after having applied "
		 << N << " qft-inverse qft pairs \n"
		 << global_state;


	return 0;
}
