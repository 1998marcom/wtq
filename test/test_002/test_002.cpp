#include "common.hpp"
#include "containers/localstate.hpp"
#include "containers/globalstate.hpp"
#include "operators/pauli.hpp"
#include <complex>
#include <cmath>
#include <vector>
#include <iostream>

using namespace std;

int main() {

	// GlobalState instantiation
	wtq::GlobalState global_state;

	// Variable instantiation (might be entangled)
	wtq::QVariable A = global_state.add_variable(2); // Add a variable with 2 qubits
	//cout << global_state;

	// Variable initialization
	wtq::SparseVectorXcf A_initial_state(4);
	A_initial_state.coeffRef(2) = complex<float>(1.0/sqrt(2.0),0.0); // <10|A>
	A_initial_state.coeffRef(3) = complex<float>(1.0/sqrt(2.0),0.0); // <11|A>
	A.reset(A_initial_state);
	//cout << global_state;

	// Operation Pauli Z
	A[1] *= wtq::PauliZ; 
	cout << "Printing global state after having applied PauliZ on qubit 1 of |10> + |11> \n"
		 << global_state;

	// Operation Pauli Y
	A[0] *= wtq::PauliY;
	cout << "Measuring A[0] after having applied PauliY to |1>: " 
		 << A[0].measure() << endl;
	
	return 0;
}
