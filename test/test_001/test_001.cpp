#include "common.hpp"
#include "containers/localstate.hpp"
#include "containers/globalstate.hpp"
#include "operators/utils.hpp"
#include "operators/pauli.hpp"
#include <complex>
#include <cmath>
#include <vector>
#include <iostream>

using namespace std;

int main() {

	// GlobalState instantiation
	wtq::GlobalState global_state;

	// Variable instantiation (might be entangled)
	wtq::QVariable A = global_state.add_variable(2); // Add a variable with 2 qubits
	wtq::QVariable B = global_state.add_variable(8); // Add a variable with 8 qubits
	wtq::QVariable C = global_state.add_variable(40); // Add a variable with 40 qubits
	//cout << global_state << endl;

	// Variable initialization
	wtq::SparseVectorXcf A_initial_state(4);
	A_initial_state.coeffRef(2) = complex<float>(1.0/sqrt(2.0),0.0); // <10|A>
	A_initial_state.coeffRef(3) = complex<float>(1.0/sqrt(2.0),0.0); // <11|A>
	A.reset(A_initial_state);
	//cout << global_state << endl;

	wtq::SparseVectorXcf B_initial_state(wtq::int_pow(2, 8));
	B_initial_state.coeffRef(0) = complex<float>(1.0, 0.0);
	B.reset(B_initial_state);

	// Operation U between A[1] and B[0]
	wtq::QVariable a1b0 = wtq::QVariable(A[1], B[0]);
	wtq::MatrixXcf U(4,4);
	U << 1, 0, 0, 0,
	     0, 1, 0, 0,
		 0, 0, 0, 1,
		 0, 0, 1, 0; // C-NOT 
	a1b0 *= U; // Note that the whole system is affected by the U multiplication
			   // and a copy-on-write or returning a qvariable of another system
			   // would be tremendously resource-hungry. As such, and to remind
			   // you of the way quantum systems work, only the *= operator is
			   // available
	
	// Equivalent code using built-in pauli matrixes and c-u
	wtq::ControlU c_not = wtq::ControlU(wtq::PauliX); // C-NOT
	c_not(A[1], B[0]);

	// QBits are often implicitly casted to QVariables, so the previous line
	// really means:
	c_not(A[1], wtq::QVariable(B[0])); 

	//cout << global_state << endl;
	
	// Measuring Qbits
	vector<int> A_measure = A.measure();
	int B0_measure = B[0].measure();
	
	cout << "Measured A: " << A_measure[0] << " " << A_measure[1] << endl;
	cout << "Measured B[0]: " << B0_measure << endl;

	return 0;
}
