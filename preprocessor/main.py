#!/usr/bin/env python3
# Imports and argparsing

import argparse, re
from pathlib import Path

parser = argparse.ArgumentParser(
    description='Preprocessor interpreter (almost pure python, with few envvars)'
)
parser.add_argument(
    '-r', '--read', nargs='*', 
    default=[],
    help='Also read this files before preprocessing, but do not output them',
)
parser.add_argument(
    '-i', '--input', nargs='+', 
    required=True,
    help='List of input files to preprocess. For dirs, all the dir subtree will be processed.',
)
parser.add_argument(
    '-o', '--output', nargs='+', required=True,
    help='''
Output file path(s) (usually in build/ dir).
If one file path or a non-existent path is specified, 
cat all processed input into a single file.
If else one directory path is specified, rebuild the dir hierarchy as output.
Else, you may specify one output file for each input file, in the same order.
    ''',
)
args = parser.parse_args()

rebuild_dir_hierarchy = (len(args.output) == 1 and Path(args.output[0]).is_dir())
cat_all_processed = (
    len(args.output) == 1 and 
    not Path(args.output[0]).exists() or not Path(args.output[0]).is_dir()
)


# Preprocess code
TRIGGER_STRING = 'PYTHON_PREPROCESSOR'

_print = print

def preprocess(code, reading=False, output=None):

    if TRIGGER_STRING in code:

        # Macro finder
        pretrigger_end = code.index(TRIGGER_STRING)
        triggered_start = pretrigger_end + len(TRIGGER_STRING) + 3
        nested = 1; last_index = triggered_start
        while nested:
            m = re.search('.*?(\{\{\{|\}\}\}).*', code[last_index:])
            if m.group(1) == '{{{':
                nested += 1
            else:
                nested -= 1
                last_index += m.end(1)
        triggered_end = last_index - 3
        posttrigger_start = last_index
        
        pretrigger = code[:pretrigger_end]
        triggered = code[triggered_start:triggered_end]
        posttrigger = code[posttrigger_start:]

        # Code preprocessing
        outcode = pretrigger
        aux_list = []
        """
        output = {
            "pretrigger": pretrigger,
            "executed": "",
        }
        """

        # Code to read - start
        if not reading:
            readcode = ""
            for readfile in args.read:
                readcode += open(readfile, 'r').read()
            preprocess(readcode, reading=True)
        # Code to read - end
        preexec = preprocess(triggered)
        m = re.match('(\s+)', preexec)
        if m:
            preexec = preexec.replace(m.group(1), '\n')
        def output(*args, **kwargs):
            aux_list.append("".join(args)+'\n')
        exec(preexec, globals(), {**locals(), 'output': output})
        outcode += "".join(aux_list)
        #else:
        #    exec(preexec, globals())
        outcode += preprocess(posttrigger)

        return outcode
        #output['pretrigger'] + output['executed'] + output['posttrigger']

    else:

        return code


if cat_all_processed:
    def recurse_cat(file):
        incode = ""
        if Path(file).is_file():
            incode += open(file, 'r').read()
        else:
            for f in Path(file).iterdir():
                incode += recurse_cat(f)
        return incode
    incode = ""
    for infile in args.input:
        incode += recurse_cat(infile)
    outcode = preprocess(incode)
    open(args.output[0], 'w').write(outcode)

elif rebuild_dir_hierarchy:
    def recurse_process(file, relative_path):
        _print("\tProcessing", relative_path)
        if Path(file).is_dir():
            (Path(args.output[0])/relative_path).mkdir(exist_ok=True, parents=True)
            for f in Path(file).iterdir():
                recurse_process(f, relative_path+'/'+f.name)
        else:
            if Path(file).name.startswith('.'):
                return
            outcode = preprocess(open(file, 'r').read())
            open(Path(args.output[0])/relative_path, 'w').write(outcode)
    for infile in args.input:
        recurse_process(infile, Path(infile).resolve().name)
    
else:
    # one output for one input
    # TODO
    raise NotImplemented("AAAAAA, This preprocessor mode is not yet implemented, multiple outputs not yet supported!")

