#include "containers/globalstate.hpp"
#include "containers/localstate.hpp"
#include <vector>
#include <memory>
#include <chrono>
#include <random>
#include <iostream>


namespace wtq {
	std::mt19937* global_generator = NULL;
	void random_init() {
		if (not global_generator) {
			auto seed = std::chrono::system_clock::now().time_since_epoch().count();
			global_generator = new std::mt19937(seed);
		}
	}

	GlobalState::GlobalState() {
		global_state_ptr = this;
		TensorPair tp; tp.probability = 1.0;
		data.push_back(tp);
	}
	QVariable GlobalState::add_variable(const int& bit_width) {
		int previous_size = own_bit_addresses.size();
		auto qvar_bit_addresses = std::vector<short>();
		for(int i=previous_size; i<bit_width+previous_size; i++) {
			own_bit_addresses.push_back(i);
			qvar_bit_addresses.push_back(i);
		}
		auto& tensor_ensemble = this->data;
		for(auto& tensor_pair: tensor_ensemble) {
			double prob = tensor_pair.probability;
			TensorState& tensor_state = tensor_pair.state;
			std::shared_ptr<LocalState> new_local_state_ptr = 
				std::make_shared<LocalStateSparseState>(this, qvar_bit_addresses);
			std::dynamic_pointer_cast<LocalStateSparseState>(new_local_state_ptr)
				->_get_data().coeffRef(0) = cf(1.0,0.0);
			// Note that the state is not initialized: undefined behaviour if
			// used without proper initialization (most likely some Eigen error)
			tensor_state.push_back(new_local_state_ptr);
		}
		return QVariable(this, qvar_bit_addresses);
	}
	int GlobalState::measure(const short& address, const int& force_result) {
		double prob = get_value(address);
		int result;

		if (force_result == -1) {
			random_init();
			std::bernoulli_distribution distribution(prob); // Extract 1 (or 0)
															// with prob (or 1-prob)
			result = distribution(*global_generator);
		}
		else {
			result = force_result;
		}
		collapse(address, result); //removes address from self; 
								   //might throw if forcing an impossible result
		SparseVectorXcf data_v(2);
		if (result == 0) data_v.coeffRef(0) = cf(1.0, 0.0);
		if (result == 1) data_v.coeffRef(1) = cf(1.0, 0.0);
		for(TensorPair& tp: data) {
			std::shared_ptr<LocalState> new_ls_ptr =
				std::make_shared<LocalStateSparseState>(this, std::vector<short>(1, address));
			std::dynamic_pointer_cast<LocalStateSparseState>(new_ls_ptr)->_get_data() = data_v;
			tp.state.push_back(new_ls_ptr);
		}
		own_bit_addresses.push_back(address);
		this->smart_reset(QVariable(this, std::vector<short>(1, address)), data_v);
		return result;
	}
	PYTHON_PREPROCESSOR{{{output(MCWS("""
	void GlobalState::apply_Us(const QVariable& qvar, const MatrixTypeCombination& mc) {
		this->apply_rUs(qvar, mc);
		this->apply_lUs(qvar, mc);
	}
	void GlobalState::apply_exp(const QVariable& qvar, const MatrixType& H) {
		this->apply_rexp(qvar, H);
		this->apply_lexp(qvar, H);
	}
	"""))}}}
}
