#include "containers/localstate.hpp"
#include <algorithm>
#include <map>
#include <set>
#include <cmath>
#include <iostream>
#include <chrono>
#include <bitset>

using namespace std;
using namespace std::chrono;

namespace wtq {
	// LocalState
	std::string LocalState::class_type() {
		return std::string("LocalState");
	}
	LocalState::LocalState(
			GlobalState* in_global_state_ptr, 
			const std::vector<short>& own_bit_addresses
	) {
		global_state_ptr = in_global_state_ptr;
		this->own_bit_addresses = own_bit_addresses;
	}
	LocalState::LocalState(const LocalState& ls, const std::vector<short>& own_bit_addresses)
		: LocalState(ls._get_global_state_ptr(), own_bit_addresses) {}
	std::vector<short> LocalState::get_own_bit_addresses() const {
		return std::vector(this->own_bit_addresses);
	}
	GlobalState* LocalState::_get_global_state_ptr() const {
		return this->global_state_ptr;
	}
	ll ix_map_to_addendum(
		const std::map<short, ll>& qubits_addendum_map,
		const ll& untouched_ix
	) {
		ll addendum = 0;
		ll index = 0;
		for (
			auto it=qubits_addendum_map.begin(); 
			it!=qubits_addendum_map.end(); 
			++it
		) {
			auto bit_address = it->first;
			auto potential_addendum = it->second;
			bool bit_is_one = (((ll)1) << index) & untouched_ix;
			if (bit_is_one) addendum += potential_addendum;
			index++;
		}
		return addendum;
	}
	ll addendum_to_ix(
		const std::map<short, ll>& qubits_addendum_map,
		const ll& addendum
	) {
		std::set<short> result;
		ll ix = 0;
		ll index = 0;
		for (
			auto it=qubits_addendum_map.begin(); 
			it!=qubits_addendum_map.end(); 
			++it
		) {
			auto bit_address = it->first;
			auto potential_addendum = it->second;
			bool bit_is_one = addendum & potential_addendum;
			if (bit_is_one) ix += (((ll)1) << index);
			++index;
		}
		return ix;

	}

	template<typename MatrixT>
	MatrixT LocalState::get_complete_U(const QVariable& input, const MatrixXcf& U) {

		ll result_size = int_pow((ll) 2, own_bit_addresses.size());
		MatrixT result(result_size, result_size); result.setZero();

		std::map<short, ll> untouched_qubits_addendum;
		std::map<short, ll> touched_qubits_new_addendum;
		std::map<short, ll> touched_qubits_old_addendum;
		// Initialize the variables above - and remember the schifo big-endian we
		// chose
		FOR(i, own_bit_addresses.size()) {
			const auto bit_address = own_bit_addresses[i];
			ll new_addendum = (((ll)1) << own_bit_addresses.size()-i-1);
			const std::vector<short> input_addresses = input.get_addresses();
			auto input_find_bit_address = std::find(
				input_addresses.begin(),
				input_addresses.end(),
				bit_address
			);
			if (input_find_bit_address != input_addresses.end()) {
				int input_bit_address_ix = input_find_bit_address - input_addresses.begin();
				ll old_addendum = (((ll)1) << input_addresses.size()-input_bit_address_ix-1);
				touched_qubits_new_addendum[bit_address] = new_addendum;
				touched_qubits_old_addendum[bit_address] = old_addendum;
			}
			else {
				untouched_qubits_addendum[bit_address] = new_addendum;
			}
		}

		
		ll maximum_untouched = int_pow(2, untouched_qubits_addendum.size());
		ll maximum_touched = int_pow(2, touched_qubits_old_addendum.size());

		std::vector<Triplet<cf>> triplets;
		FORL(untouched_ix, maximum_untouched) {
			ll new_addendum_all = ix_map_to_addendum(untouched_qubits_addendum, untouched_ix);
			FORL(touched_ix, maximum_touched) {
				ll new_addendum_partR = ix_map_to_addendum(touched_qubits_new_addendum, touched_ix);
				ll old_addendum_partR = ix_map_to_addendum(touched_qubits_old_addendum, touched_ix);
				FORL(touched_jx, maximum_touched) {
					ll new_addendum_partC = ix_map_to_addendum(touched_qubits_new_addendum, touched_jx);
					ll old_addendum_partC = ix_map_to_addendum(touched_qubits_old_addendum, touched_jx);
					if (U(old_addendum_partR, old_addendum_partC) != cf(0.0, 0.0)) {
						if (typeid(MatrixT) != typeid(SparseMatrixXcf)) {
							result.coeffRef(
								(ll) (new_addendum_all + new_addendum_partR),
								(ll) (new_addendum_all + new_addendum_partC)
							) = U(old_addendum_partR, old_addendum_partC);
						}
						else {
							triplets.push_back(Triplet<cf>(
									(ll) (new_addendum_all + new_addendum_partR), 
									(ll) (new_addendum_all + new_addendum_partC), 
									U(old_addendum_partR, old_addendum_partC)
							));
						}
					}
				}
			}
		}
		if constexpr (std::is_same<MatrixT, SparseMatrixXcf>::value) {
			result.setFromTriplets(triplets.begin(), triplets.end());
		}
		return result;
	}

	template<typename MatrixT>
	MatrixT LocalState::get_complete_U(const QVariable& input, const SparseMatrixXcf& U) {

		ll result_size = int_pow((ll) 2, own_bit_addresses.size());
		MatrixT result(result_size, result_size); result.setZero();

		std::map<short, ll> untouched_qubits_addendum;
		std::map<short, ll> touched_qubits_new_addendum;
		std::map<short, ll> touched_qubits_old_addendum;
		// Initialize the variables above - and remember the schifo big-endian we
		// chose
		FOR(i, own_bit_addresses.size()) {
			const auto bit_address = own_bit_addresses[i];
			ll new_addendum = (((ll)1) << own_bit_addresses.size()-i-1);
			const std::vector<short> input_addresses = input.get_addresses();
			auto input_find_bit_address = std::find(
				input_addresses.begin(),
				input_addresses.end(),
				bit_address
			);
			if (input_find_bit_address != input_addresses.end()) {
				int input_bit_address_ix = input_find_bit_address - input_addresses.begin();
				ll old_addendum = (((ll)1) << input_addresses.size()-input_bit_address_ix-1);
				touched_qubits_new_addendum[bit_address] = new_addendum;
				touched_qubits_old_addendum[bit_address] = old_addendum;
			}
			else {
				untouched_qubits_addendum[bit_address] = new_addendum;
			}
		}

		ll maximum_untouched = int_pow(2, untouched_qubits_addendum.size());

		std::vector<Triplet<cf>> triplets;
		FORL(untouched_ix, maximum_untouched) {
			ll new_addendum_all = ix_map_to_addendum(untouched_qubits_addendum, untouched_ix);
			FORL(k, U.outerSize()) {
				for (SparseMatrixXcf::InnerIterator it(U,k); it; ++it) {
					cf value = it.value();
					auto old_row = it.row();   // row index
					auto old_col = it.col();   // col index
					ll ix = (ll) addendum_to_ix(touched_qubits_old_addendum, old_row);
					ll jx = (ll) addendum_to_ix(touched_qubits_old_addendum, old_col);
					ll new_addendum_R = ix_map_to_addendum(touched_qubits_new_addendum, ix);
					ll new_addendum_C = ix_map_to_addendum(touched_qubits_new_addendum, jx);
					ll new_row = new_addendum_all + new_addendum_R;
					ll new_col = new_addendum_all + new_addendum_C;
					if (typeid(MatrixT) != typeid(SparseMatrixXcf)) {
						result.coeffRef(new_row, new_col) = value;
					}
					else {
						triplets.push_back(Triplet<cf>(new_row, new_col, value));
					}
				}
			}
		}

		if constexpr (std::is_same<MatrixT, SparseMatrixXcf>::value) {
			result.setFromTriplets(triplets.begin(), triplets.end());
		}
		return result;
		
	}
	PYTHON_PREPROCESSOR{{{output(MCWS(
	"""
	template MatrixType LocalState::get_complete_U<MatrixType>(const QVariable& input, const MatrixXcf& U);
	template MatrixType LocalState::get_complete_U<MatrixType>(const QVariable& input, const SparseMatrixXcf& U);
	"""
	))}}}
	/** Given a certain address, this function returns how much to add to the
	 * data index to obtain a data item representative of a state in which the
	 * bit at address `address` is 1.
	 */
	ll LocalState::address_to_index_addendum(
		const short& address, 
		const std::vector<short>& bit_addrs//=std::vector<short>()
	) const { 
		auto bit_addresses = this->own_bit_addresses;
		if (bit_addrs.size() > 0) {
			bit_addresses = bit_addrs;
		}
		ll bit_binary_value = ((ll)1) << (
			bit_addresses.end() 
			- std::find(bit_addresses.begin(), bit_addresses.end(), address) 
			- 1
		);
		return bit_binary_value;
	}
	void LocalState::check_collapse_possibility(const short& address, const int& value) const {
		double prob = this->get_value(address);
		bool impossibility = (prob==1.0 and value==0 or prob==0.0 and value==1);
		if (impossibility) {
			throw std::runtime_error("Asked for an impossible collapse");
		}
	}

	// LocalStateSparseState
	LocalStateSparseState::LocalStateSparseState(
			GlobalState* in_global_state_ptr, 
			const std::vector<short>& own_bit_addresses,
			const SparseVectorXcf& in_data
	) : LocalStateSparseState(in_global_state_ptr, own_bit_addresses) {
		data = in_data;
	}
	SparseVectorXcf& LocalStateSparseState::_get_data() {
		if (data.size() != (((ll)1)<<own_bit_addresses.size()))
			data.resize(((ll)1)<<own_bit_addresses.size());
		return data;
	}
	const SparseVectorXcf& LocalStateSparseState::_get_const_data() const {
		return data;
	}
	std::string LocalStateSparseState::class_type() {
		return std::string("LocalStateSparseState");
	}
	void LocalStateSparseState::apply_U(const SparseMatrixXcf& U) {	
		data = U * data; 

		// equivalent of data.normalize()
		data = data * 1.0/sqrt(data.cwiseProduct(data.adjoint().transpose()).sum().real());
	}
	PYTHON_PREPROCESSOR{{{output(MCWS(
	"""
	void LocalStateSparseState::apply_U(const QVariable& input, const MatrixType& U) {
		//if (get_own_bit_addresses().size() > input.get_addresses().size() + 1) {
			SparseMatrixXcf complete_U = get_complete_U<SparseMatrixXcf>(input, U);
			apply_U(complete_U);
		//}
		/*else {
			MatrixXcf complete_U = get_complete_U<MatrixXcf>(input, U);
			apply_U(complete_U);
		}*/
	}
	void LocalStateSparseState::apply_lUs(
		const QVariable& qvar, 
		const MatrixTypeCombination& mc
	) {
		auto start = high_resolution_clock::now();
		//cout << "SparseMultiply lUs start" << std::endl;
		auto original_data = data;
		data.setZero();
		//cout << "SparseMultiply lUs start" << std::endl;
		MatrixType old_mat(qvar.data_size(), qvar.data_size());
		old_mat.setZero();
		SparseMatrixXcf complete_mat = get_complete_U<SparseMatrixXcf>(qvar, old_mat);
		for(const auto& pair: mc) {
		//cout << "SparseMultiply lUs B, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
			auto editable_data = original_data;
		//cout << "SparseMultiply lUs C, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
			auto& coeff = pair.first;
			editable_data *= coeff;
		//cout << "SparseMultiply lUs D, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
			auto& matrix_list = pair.second;
			for(const auto& mat: matrix_list) {
		//cout << "SparseMultiply lUs E, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
				if (not (mat.isApprox(old_mat))) {
					complete_mat = get_complete_U<SparseMatrixXcf>(qvar, mat);
					old_mat = mat;
				}
		//cout << "SparseMultiply lUs F, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		//cout << "editable_data.size" << editable_data.size() << endl;
				editable_data = complete_mat * editable_data;
			}
			data += editable_data;
		//cout << "SparseMultiply lUs G, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		}
		//cout << "SparseMultiply lUs done, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;

		// equivalent of data.normalize()
		data = data * 1.0/sqrt(data.cwiseProduct(data.adjoint().transpose()).sum().real());
	}
	void LocalStateSparseState::apply_lexp(
		const QVariable& qvar,
		const MatrixType& H
	) {
        //Build matrix combination
        auto lambda_max_est = estimate_lambda_max(H);
        auto r_trotter = (int) (17.0/2.0*lambda_max_est);
        int exp_taylor_order = (int) 2.0*log10(lambda_max_est)+13;
        //int exp_taylor_order = (int) (17.0*lambda_max_est+9);
		SparseMatrixXcf complete_H = get_complete_U<SparseMatrixXcf>(qvar, H);

		FOR(_, r_trotter) {
			auto progressive_vector = data;
			data.setZero();

			FOR(order, exp_taylor_order+1) {
				cf coeff = std::pow(
						cf(0,1) / ((double) r_trotter),
						order
					) / ((double) factorial(order));
				data += coeff * progressive_vector;
				progressive_vector = complete_H * progressive_vector;
			}
			// equivalent of data.normalize()
			data = data * 1.0/sqrt(data.cwiseProduct(data.adjoint().transpose()).sum().real());
		}
	}
	"""
	))}}}
	LocalStateSparseState::LocalStateSparseState(
		const LocalStateSparseState& ls1, 
		const LocalStateSparseState& ls2
	) : LocalStateSparseState(ls1._get_global_state_ptr(), std::vector<short>()){
		auto ls1_bit_addresses = ls1.get_own_bit_addresses();
		auto ls2_bit_addresses = ls2.get_own_bit_addresses();
		own_bit_addresses = ls1_bit_addresses;
		own_bit_addresses.insert(
			own_bit_addresses.end(), 
			ls2_bit_addresses.begin(), 
			ls2_bit_addresses.end()
		);
		data.resize(((ll)1)<<own_bit_addresses.size());
		const auto ls1_data = ls1._get_const_data(); const auto ls2_data = ls2._get_const_data();
		for (SparseVectorXcf::InnerIterator it(ls1_data); it; ++it) {
			for (SparseVectorXcf::InnerIterator jt(ls2_data); jt; ++jt) {
				auto position = it.index() * ls2_data.size() + jt.index();
				data.coeffRef(position) = it.value() * jt.value();
			}
		}
		// equivalent of data.normalize()
		data = data * 1.0/sqrt(data.cwiseProduct(data.adjoint().transpose()).sum().real());
	}
	double LocalStateSparseState::get_value(const short& address) const {
		// Guaranteed address from dispatch
		double value = 0.0;
		/**
		std::cout << "Getting value from LocalStateSparseState... " 
				  << "for address = " << address << std::endl;
		*/
		for (SparseVectorXcf::InnerIterator it(data); it; ++it) {
		/**
			std::cout << "Iterating over index " << it.index()
					  << " with address_to_index_addendum(address) = " 
					  << address_to_index_addendum(address)
					  << std::endl;
		*/
			bool bit_is_one = it.index() & address_to_index_addendum(address);
			value += bit_is_one * std::norm(it.value());
		}
		return value;
	}
	void LocalStateSparseState::collapse(const short& address, const int& value) {
		// Check for impossibility of collapse
		check_collapse_possibility(address, value);
		SparseVectorXcf new_data;
		std::vector<short> new_bit_addresses = own_bit_addresses;
		new_bit_addresses.erase(
			std::find(new_bit_addresses.begin(), new_bit_addresses.end(), address)
		);
		new_data.resize(((ll)1)<<new_bit_addresses.size());
		for (SparseVectorXcf::InnerIterator it(data); it; ++it) {
			bool bit_is_one = it.index() & address_to_index_addendum(address);
			//Here are bit operations to translate the older index into the
			//new one
			int address_log_value = 
				own_bit_addresses.end()
				- std::find(own_bit_addresses.begin(), own_bit_addresses.end(), address)
				- 1
			;
			ll new_index = 
				  (it.index() & (address_to_index_addendum(address)-((ll)1)))
				+ ((it.index() >> (address_log_value+((ll)1))) << address_log_value)
			;
			if (not value ^ bit_is_one) {
				new_data.coeffRef(new_index) = it.value();
			}
		}
		data = new_data;
		own_bit_addresses = new_bit_addresses;
		data.prune(EPSILON); // Only almost zeros are pruned
		// equivalent of data.normalize()
		data = data * 1.0/sqrt(data.cwiseProduct(data.adjoint().transpose()).sum().real());
	}

	// LocalStateDenseState
	std::string LocalStateDenseState::class_type() {
		return std::string("LocalStateDenseState");
	}
	VectorXcf& LocalStateDenseState::_get_data() {
		if (data.size() != (((ll)1)<<own_bit_addresses.size()))
			data.resize(((ll)1)<<own_bit_addresses.size());
		return data;
	}
	const VectorXcf& LocalStateDenseState::_get_data() const {
		return data;
	}

	LocalStateDenseState::LocalStateDenseState(
			GlobalState* in_global_state_ptr, 
			const std::vector<short>& own_bit_addresses,
			const VectorXcf& in_data
	) : LocalStateDenseState(in_global_state_ptr, own_bit_addresses) {
		data = in_data;
	}
	LocalStateDenseState::LocalStateDenseState(const LocalStateSparseState& input) {
		global_state_ptr = input._get_global_state_ptr();
		own_bit_addresses = input.get_own_bit_addresses();
		const auto input_data = input._get_const_data();
		data.setZero();
		for(SparseVectorXcf::InnerIterator it(input_data); it; ++it) {
			data[it.index()] = it.value();
		}
	}
	LocalStateDenseState::LocalStateDenseState(
		const LocalStateDenseState& ls1,
		const LocalStateDenseState& ls2
	) : LocalStateDenseState(ls1._get_global_state_ptr(), std::vector<short>()) {
		auto ls1_bit_addresses = ls1.get_own_bit_addresses();
		auto ls2_bit_addresses = ls2.get_own_bit_addresses();
		own_bit_addresses = ls1_bit_addresses;
		own_bit_addresses.insert(
			own_bit_addresses.end(), 
			ls2_bit_addresses.begin(), 
			ls2_bit_addresses.end()
		);
		data.resize(((ll)1)<<own_bit_addresses.size());
		const auto ls1_data = ls1._get_data(); const auto ls2_data = ls2._get_data();
		FORL(i, ls1_data.size()) {
			FORL(j, ls2_data.size()) {
				ll position = i * ls2_data.size() + j;
				data[position] = ls1_data[i] * ls2_data[j];
			}
		}
		data.normalize();
	}
	PYTHON_PREPROCESSOR{{{output(MCWS(
	"""
	void LocalStateDenseState::apply_U(const MatrixType& U) {	data = data * U; }
	void LocalStateDenseState::apply_U(const QVariable& input, const MatrixType& U) {
		if (get_own_bit_addresses().size() > input.get_addresses().size() + 1) {
			SparseMatrixXcf complete_U = get_complete_U<SparseMatrixXcf>(input, U);
			apply_U(complete_U);
		}
		else {
			MatrixXcf complete_U = get_complete_U<MatrixXcf>(input, U);
			apply_U(complete_U);
		}
	}
	void LocalStateDenseState::apply_lUs(
		const QVariable& qvar, 
		const MatrixTypeCombination& mc
	) {
		// TO CHECK
		auto original_data = data;
		data.setZero();
		for(const auto& pair: mc) {
			auto editable_data = original_data;
			auto& coeff = pair.first;
			editable_data *= coeff;
			auto& matrix_list = pair.second;
			for(const auto& mat: matrix_list) {
				SparseMatrixXcf complete_mat = get_complete_U<SparseMatrixXcf>(qvar, mat);
				editable_data = complete_mat * editable_data;
			}
			data += editable_data;
		}
	}
	"""
	))}}}
	double LocalStateDenseState::get_value(const short& address) const {
		// Guaranteed address from dispatch
		double value = 0.0;
		for (auto it=data.begin(); it!=data.end(); it++) {
			bool bit_is_one = (it-data.begin()) & address_to_index_addendum(address);
			value += bit_is_one * std::norm(*it);
		}
		return value;
	}
	void LocalStateDenseState::collapse(const short& address, const int& value) {
		// Check for impossibility of collapse
		check_collapse_possibility(address, value);
		/** TO TRASH
		for (auto it=data.begin(); it!=data.end(); it++) {
			bool bit_is_one = (it-data.begin()) & address_to_index_addendum(address);
			*it *= (double) ~(value ^ bit_is_one);
		}
		own_bit_addresses.erase(
			std::find(own_bit_addresses.begin(), own_bit_addresses.end(), address)
		);
		**/
		VectorXcf new_data;
		std::vector<short> new_bit_addresses = own_bit_addresses;
		new_bit_addresses.erase(
			std::find(new_bit_addresses.begin(), new_bit_addresses.end(), address)
		);
		new_data.resize(((ll)1)<<new_bit_addresses.size());
		FOR(index, data.size()) {
			bool bit_is_one = index & address_to_index_addendum(address);
			//Here are bit operations to translate the older index into the
			//new one
			int address_log_value = 
				own_bit_addresses.end()
				- std::find(own_bit_addresses.begin(), own_bit_addresses.end(), address)
				- 1
			;
			ll new_index = 
				  (index & (address_to_index_addendum(address)-1))
				+ ((index >> (address_log_value+1)) << address_log_value)
			;
			if (not value ^ bit_is_one) {
				new_data.coeffRef(new_index) = data[index];
			}
		}
		data = new_data;
		own_bit_addresses = new_bit_addresses;
		data.normalize();
	}

	// LocalStateDensityMatrix
	std::string LocalStateDensityMatrix::class_type() {
		return std::string("LocalStateDensityMatrix");
	}
	MatrixXcf& LocalStateDensityMatrix::_get_data() {
		if (data.rows() != (((ll)1)<<own_bit_addresses.size()) or
				data.cols() != (((ll)1)<<own_bit_addresses.size())) {
			data.resize(
				((ll)1)<<own_bit_addresses.size(),
				((ll)1)<<own_bit_addresses.size()
			);
		}
		return data;
	}
	LocalStateDensityMatrix::LocalStateDensityMatrix(
		const LocalStateDensityMatrix& ls1,
		const LocalStateDensityMatrix& ls2
	) : LocalStateDensityMatrix(ls1._get_global_state_ptr(), std::vector<short>()) {
		auto ls1_bit_addresses = ls1.get_own_bit_addresses();
		auto ls2_bit_addresses = ls2.get_own_bit_addresses();
		own_bit_addresses = ls1_bit_addresses;
		own_bit_addresses.insert(
			own_bit_addresses.end(), 
			ls2_bit_addresses.begin(), 
			ls2_bit_addresses.end()
		);
		//TODO
	}
	PYTHON_PREPROCESSOR{{{output(MCWS("""
	void LocalStateDensityMatrix::apply_U(const QVariable& input, const MatrixType& U) {
		//TODO
	}
	void LocalStateDensityMatrix::apply_lUs(
		const QVariable& qvar, 
		const MatrixTypeCombination& mc
	) {
		// TO CHECK
		auto original_data = data;
		data.setZero();
		for(const auto& pair: mc) {
			auto editable_data = original_data;
			auto& coeff = pair.first;
			editable_data *= coeff;
			auto& matrix_list = pair.second;
			for(const auto& mat: matrix_list) {
				SparseMatrixXcf complete_mat = get_complete_U<SparseMatrixXcf>(qvar, mat);
				editable_data = complete_mat * editable_data;
			}
			data += editable_data;
		}
	}
	void LocalStateDensityMatrix::apply_rUs(
		const QVariable& qvar, 
		const MatrixTypeCombination& mc
	) {
		// TO CHECK
		auto original_data = data;
		data.setZero();
		for(const auto& pair: mc) {
			auto editable_data = original_data;
			auto& coeff = pair.first;
			editable_data *= coeff;
			auto& matrix_list = pair.second;
			for(const auto& mat: matrix_list) {
				SparseMatrixXcf complete_mat = get_complete_U<SparseMatrixXcf>(qvar, mat);
				editable_data = editable_data * complete_mat;
			}
			data += editable_data;
		}
	}
	"""))}}}
	double LocalStateDensityMatrix::get_value(const short& address) const {
		//TODO
		return 0.0;
	}
	void LocalStateDensityMatrix::collapse(const short& address, const int& value) {
		// Check for impossibility of collapse
		check_collapse_possibility(address, value);
		//TODO
		own_bit_addresses.erase(
			std::find(own_bit_addresses.begin(), own_bit_addresses.end(), address)
		);
	}

	// LocalStateTensorEnsemble
	std::string LocalStateTensorEnsemble::class_type() {
		return std::string("LocalStateTensorEnsemble");
	}
	PYTHON_PREPROCESSOR{{{output(MCWS(
	"""
	void LocalStateTensorEnsemble::apply_U(const QVariable& input, const MatrixType& U) {
		auto qvar_addresses = input.get_addresses();
		
		// STEP 0:
		// Iterate over TensorPair-s (and process the easy ones)
		TensorEnsemble still_to_process;
		for(auto tensor_pair_it = data.begin(); tensor_pair_it != data.end(); ) {
			auto prob = (*tensor_pair_it).probability;
			auto tensor_state = (*tensor_pair_it).state;

			// STEP 1:
			// Make vector of involved sub-local states
			TensorState involved_sublocal_states =
				find_involved_sublocal_states(input, tensor_state);

			// STEP 2, case A: 
			// involved_sublocal_states has just one element
			// then let's pass the struggle to it
			if (involved_sublocal_states.size() == 1) {
				involved_sublocal_states[0]->apply_U(input, U);
				tensor_pair_it++;
				continue;
			}

			// STEP 2, case B:
			// involved_sublocal_states has more than one element 
			// (note that there should be ofc at least one element)
			// We leave this tensor_pair to be processed later 
			// (and we remove this tensor_pair now from the ensemble)
			else {
				still_to_process.push_back(*tensor_pair_it);
				data.erase(tensor_pair_it);
			}
		}

		// Case 2B further processing

		// STEP 2B-1
		// Ensure the TensorEnsemble to be processed is flattened
		still_to_process = still_to_process.flatten();

		// STEP 2B-2
		// For each TensorState in a TensorPair, merge involved sublocal states
		// and apply U to involved substates. Replace old involved substates
		// with the new merged data.
		for(auto& tp: still_to_process) {
			TensorState& ts = tp.state;
			TensorState involved_sublocal_states =
				find_involved_sublocal_states(input, ts);
			for(auto ts_it = ts.begin(); ts_it != ts.end(); ) {
				if (contains(*ts_it, involved_sublocal_states)) {
					ts.erase(ts_it);
				}
				else {
					ts_it++;
				}
			}
			TensorState merged_involved_states = magic_merge(involved_sublocal_states);
			merged_involved_states[0]->apply_U(input, U);
			ts.push_back(merged_involved_states[0]);
		}
		data.insert(data.end(), still_to_process.begin(), still_to_process.end());
	}

	/**
	void LocalStateTensorEnsemble::apply_cU(
			const QBit& control_bit, 
			const QVariable& input,
			const MatrixType& U
	) {
		//TODO
	}*/
	"""
	))}}}

	// Now almost the same code written for rU and lU
	PYTHON_PREPROCESSOR{{{output(MCWS(multiply_code_while_substituting("XU", ["rUs", "lUs"],
	"""
	void LocalStateTensorEnsemble::apply_XU(const QVariable& input, const MatrixTypeCombination& mc) {
		auto qvar_addresses = input.get_addresses();
		
		// STEP 0:
		// Iterate over TensorPair-s (and process the easy ones)
		TensorEnsemble still_to_process;
		for(auto tensor_pair_it = data.begin(); tensor_pair_it != data.end(); ) {
			auto prob = (*tensor_pair_it).probability;
			auto tensor_state = (*tensor_pair_it).state;

			// STEP 1:
			// Make vector of involved sub-local states
			TensorState involved_sublocal_states =
				find_involved_sublocal_states(input, tensor_state);

			// STEP 2, case A: 
			// involved_sublocal_states has just one element
			// then let's pass the struggle to it
			if (involved_sublocal_states.size() == 1) {
				involved_sublocal_states[0]->apply_XU(input, mc);
				tensor_pair_it++;
				continue;
			}

			// STEP 2, case B:
			// involved_sublocal_states has more than one element 
			// (note that there should be ofc at least one element)
			// We leave this tensor_pair to be processed later 
			// (and we remove this tensor_pair now from the ensemble)
			else {
				still_to_process.push_back(*tensor_pair_it);
				data.erase(tensor_pair_it);
			}
		}

		// Case 2B further processing

		// STEP 2B-1
		// Ensure the TensorEnsemble to be processed is flattened
		still_to_process = still_to_process.flatten();

		// STEP 2B-2
		// For each TensorState in a TensorPair, merge involved sublocal states
		// and apply U to involved substates. Replace old involved substates
		// with the new merged data.
		for(auto& tp: still_to_process) {
			TensorState& ts = tp.state;
			TensorState involved_sublocal_states =
				find_involved_sublocal_states(input, ts);
			for(auto ts_it = ts.begin(); ts_it != ts.end(); ) {
				if (contains(*ts_it, involved_sublocal_states)) {
					ts.erase(ts_it);
				}
				else {
					ts_it++;
				}
			}
			TensorState merged_involved_states = magic_merge(involved_sublocal_states);
			merged_involved_states[0]->apply_XU(input, mc);
			ts.push_back(merged_involved_states[0]);
		}
		data.insert(data.end(), still_to_process.begin(), still_to_process.end());
	}
	""")))}}}

	// Now almost the same code written for rexp and lexp
	PYTHON_PREPROCESSOR{{{output(MCWS(multiply_code_while_substituting("Xexp", ["rexp", "lexp"],
	"""
	void LocalStateTensorEnsemble::apply_Xexp(const QVariable& input, const MatrixType& H) {
		auto qvar_addresses = input.get_addresses();
		
		// STEP 0:
		// Iterate over TensorPair-s (and process the easy ones)
		TensorEnsemble still_to_process;
		for(auto tensor_pair_it = data.begin(); tensor_pair_it != data.end(); ) {
			auto prob = (*tensor_pair_it).probability;
			auto tensor_state = (*tensor_pair_it).state;

			// STEP 1:
			// Make vector of involved sub-local states
			TensorState involved_sublocal_states =
				find_involved_sublocal_states(input, tensor_state);

			// STEP 2, case A: 
			// involved_sublocal_states has just one element
			// then let's pass the struggle to it
			if (involved_sublocal_states.size() == 1) {
				involved_sublocal_states[0]->apply_Xexp(input, H);
				tensor_pair_it++;
				continue;
			}

			// STEP 2, case B:
			// involved_sublocal_states has more than one element 
			// (note that there should be ofc at least one element)
			// We leave this tensor_pair to be processed later 
			// (and we remove this tensor_pair now from the ensemble)
			else {
				still_to_process.push_back(*tensor_pair_it);
				data.erase(tensor_pair_it);
			}
		}

		// Case 2B further processing

		// STEP 2B-1
		// Ensure the TensorEnsemble to be processed is flattened
		still_to_process = still_to_process.flatten();

		// STEP 2B-2
		// For each TensorState in a TensorPair, merge involved sublocal states
		// and apply U to involved substates. Replace old involved substates
		// with the new merged data.
		for(auto& tp: still_to_process) {
			TensorState& ts = tp.state;
			TensorState involved_sublocal_states =
				find_involved_sublocal_states(input, ts);
			for(auto ts_it = ts.begin(); ts_it != ts.end(); ) {
				if (contains(*ts_it, involved_sublocal_states)) {
					ts.erase(ts_it);
				}
				else {
					ts_it++;
				}
			}
			TensorState merged_involved_states = magic_merge(involved_sublocal_states);
			merged_involved_states[0]->apply_Xexp(input, H);
			ts.push_back(merged_involved_states[0]);
		}
		data.insert(data.end(), still_to_process.begin(), still_to_process.end());
	}
	""")))}}}

	void LocalStateTensorEnsemble::flatten() {
		data = data.flatten();
	}
	TensorEnsemble& LocalStateTensorEnsemble::_get_data() {
		return data;
	}
	double LocalStateTensorEnsemble::get_value(const short& address) const {
		if (not contains(address, this->own_bit_addresses)) {
			throw std::runtime_error("Sorry, I have no value stored for the address you requested");
		}
		double value = 0.0;
		for (TensorPair tp: data) {
			std::shared_ptr<LocalState> right_substate_ptr;
			for (std::shared_ptr<LocalState> ls_ptr: tp.state) {
				if (contains(address, ls_ptr->get_own_bit_addresses())) {
					right_substate_ptr = ls_ptr;
				}
			}
			value += tp.probability * right_substate_ptr->get_value(address);
		}
		return value;
	}
    void LocalStateTensorEnsemble::collapse(const short& address, const int& value) {
        // Check for impossibility of collapse
		check_collapse_possibility(address, value);
        own_bit_addresses.erase(
            std::find(own_bit_addresses.begin(), own_bit_addresses.end(), address)
        ); // Address to be removed
        // Feasibility guaranteed
        data.collapse(address, value);
    }
	/** TO TRASH
	std::shared_ptr<LocalState> LocalStateTensorEnsemble::collapsed(const short& address, const int& value) {
		// Check for impossibility of collapse
		check_collapse_possibility(address, value);
		// Feasibility guaranteed
		std::shared_ptr<LocalState> ptr = std::make_shared<LocalStateTensorEnsemble>(*this);
		ptr->collapse(address, value);
		return ptr;
	}*/

	PYTHON_PREPROCESSOR{{{
	for DataT, LocalStateDT in [
		('SparseVectorXcf','LocalStateSparseState'),
		('VectorXcf', 'LocalStateDenseState'),
		('MatrixXcf', 'LocalStateDensityMatrix'),
	]:
		output("""
	void LocalStateTensorEnsemble::smart_reset(const QVariable& input, const """+DataT+"""& new_data) {
		for(auto address: input.get_addresses()) {
			double address_value = get_value(address);
			int collapse_value = -1;
			if (1.0-address_value < EPSILON) collapse_value = 1;
			if (address_value < EPSILON) collapse_value = 0;

			if (collapse_value > -1) {
				collapse(address, collapse_value);
			}
			else {
				own_bit_addresses.erase(
					std::find(own_bit_addresses.begin(), own_bit_addresses.end(), address)
				); // Address to be removed
				TensorEnsemble TE0 = data;
				TensorEnsemble TE1 = data;
				TE0.collapse(address, 0); TE0 = TE0 * (1-address_value);
				TE1.collapse(address, 1); TE1 = TE1 * address_value;
				data = TE0;
				data.insert(data.end(), TE1.begin(), TE1.end());
			}
		}
		TensorState new_tensorstate;
		if (own_bit_addresses.size() > 0) {
			std::shared_ptr<LocalState> unmodified_ls_ptr = 
				std::make_shared<LocalStateTensorEnsemble>(*this);
			std::dynamic_pointer_cast<LocalStateTensorEnsemble>(unmodified_ls_ptr)->_get_data() = data;
			new_tensorstate.push_back(unmodified_ls_ptr);
		}
		std::shared_ptr<LocalState> modified_ls_ptr =
			std::make_shared<"""+LocalStateDT+""">(global_state_ptr, input.get_addresses());
		(std::dynamic_pointer_cast<"""+LocalStateDT+""">(modified_ls_ptr))->_get_data() = new_data;
		new_tensorstate.push_back(modified_ls_ptr);
		TensorPair new_tensorpair; 
		//std::cout <<"C"<<std::endl;
		new_tensorpair.probability = 1.0; new_tensorpair.state = new_tensorstate;
		data.clear(); data.push_back(new_tensorpair);
		own_bit_addresses.insert(
			own_bit_addresses.end(), 
			input.get_addresses().begin(), 
			input.get_addresses().end()
		);
	}
		""")
	}}}
	StateType LocalStateTensorEnsemble::max_state_type(const QBit& qbit) const {
		// TODO: rewrite this function using python preprocessor
		StateType st = StateType::sparse_state;
		for (const TensorPair& tp: data) {
			for (const auto& ls_ptr: tp.state) {
				std::string class_type = ls_ptr->class_type();
				if (class_type == "LocalStateDenseState")
					st = MAX(st, StateType::dense_state);
				if (class_type == "LocalStateDensityMatrix")
					st = MAX(st, StateType::density_matrix);
				if (class_type == "LocalStateTensorEnsemble")
					st = MAX(
						st, 
						std::dynamic_pointer_cast<LocalStateTensorEnsemble>(ls_ptr)
							->max_state_type(qbit)
					);
			}
		}
		return st;
	}

	// TensorEnsemble member functions
	TensorEnsemble::TensorEnsemble(TensorState& almost_flat_ts) {
		// Supposing almost_flat_ts.size()>0
		if (almost_flat_ts[0]->class_type() != "LocalStateTensorEnsemble") {
			TensorPair tp0;
			tp0.probability = 1.0;
			tp0.state = TensorState(1, std::shared_ptr<LocalState>(almost_flat_ts[0]));
			this->push_back(tp0);
		}
		else {
			TensorEnsemble& afts_data = 
				std::dynamic_pointer_cast<LocalStateTensorEnsemble>(almost_flat_ts[0])->_get_data();
			*this = afts_data;
		}
		// Next we add everything through tensorization
		for (int i=1; i<almost_flat_ts.size(); i++) {
			if (typeid(*almost_flat_ts[i]) == typeid(LocalStateSparseState))
				*this = this->tensorize(
					*std::dynamic_pointer_cast<LocalStateSparseState>(almost_flat_ts[i])
				);
			if (typeid(*almost_flat_ts[i]) == typeid(LocalStateDenseState))
				*this = this->tensorize(
					*std::dynamic_pointer_cast<LocalStateDenseState>(almost_flat_ts[i])
				);
			if (typeid(*almost_flat_ts[i]) == typeid(LocalStateDensityMatrix))
				*this = this->tensorize(
					*std::dynamic_pointer_cast<LocalStateDensityMatrix>(almost_flat_ts[i])
				);
			if (typeid(*almost_flat_ts[i]) == typeid(LocalStateTensorEnsemble))
				*this = this->tensorize(
					*std::dynamic_pointer_cast<LocalStateTensorEnsemble>(almost_flat_ts[i])
				);
		}
	}

	TensorEnsemble TensorEnsemble::flatten() const {
		TensorEnsemble temp = *this;
		for (auto it = temp.begin(); it != temp.end(); it++) {
			TensorState ts = it->state;
			for (std::shared_ptr<LocalState> ls_ptr: ts) {
				ls_ptr->flatten();
			}
		}
		TensorEnsemble result;
		for (TensorPair& tp: temp) {
			auto prob = tp.probability;
			auto ts = tp.state;
			TensorEnsemble addendum = prob*TensorEnsemble(ts);
			result.insert(result.end(), addendum.begin(), addendum.end());
		}
		return result;
	}

	PYTHON_PREPROCESSOR{{{output(MCWS(
	"""
	TensorEnsemble TensorEnsemble::tensorize(FlatLocalStateType& ls) const {
		TensorEnsemble result = *this;
		std::shared_ptr<LocalState> ls_ptr = std::make_shared<FlatLocalStateType>(ls);
		for (TensorPair& tp: result) {
			tp.state.push_back(ls_ptr);
		}
		return result;
	}
	"""
	))}}}

	TensorEnsemble TensorEnsemble::tensorize(LocalStateTensorEnsemble& ls) const {
		TensorEnsemble result;
		TensorEnsemble& ls_data = ls._get_data();
		FOR(i, this->size()) {
			FOR(j, ls_data.size()) {
				TensorPair addendum;
				addendum.probability = (*this)[i].probability * ls_data[j].probability;
				TensorState& as = addendum.state;
				as = (*this)[i].state;
				as.insert(as.end(), ls_data[j].state.begin(), ls_data[j].state.end());
				result.push_back(addendum);
			}
		}
		return result;
	}
	TensorEnsemble& TensorEnsemble::prune_and_normalize() {
		// Prune
		for(TensorEnsemble::iterator it=begin(); it!=end();) {
			if (it->probability == 0.0) {
				erase(it);
			}
			else {
				it++;
			}
		}
		// Normalize
		double total_prob = 0.0;
		for(TensorPair tp: *this) {
			total_prob += tp.probability;
		}
		for(TensorPair& tp: *this) {
			tp.probability /= total_prob;
		}
		return *this;
	}
	TensorEnsemble& TensorEnsemble::collapse(const short& address, const int& value) {
		for(TensorPair& tp: *this) {
			std::shared_ptr<LocalState> right_substate_ptr;
			for (std::shared_ptr<LocalState> ls_ptr: tp.state) {
				if (contains(address, ls_ptr->get_own_bit_addresses())) {
					right_substate_ptr = ls_ptr; break;
				}
			}
			
			double prob = right_substate_ptr->get_value(address);
			tp.probability *= value ? prob : 1-prob ;

			std::shared_ptr<LocalState> new_substate_ptr;
			PYTHON_PREPROCESSOR{{{output(
			make_shared_LocalState_as(
				"new_substate_ptr", 
				"*right_substate_ptr", 
				"*std::dynamic_pointer_cast<{localstatetype}>(right_substate_ptr)")
			)}}}
			//if (not prob ^ value) {
				if (new_substate_ptr->get_own_bit_addresses().size() > 1)
					new_substate_ptr->collapse(address, value); 
				for (auto it=tp.state.begin(); it != tp.state.end(); it++) {
					if(*it ==right_substate_ptr) {
						if (right_substate_ptr->get_own_bit_addresses().size() > 1)
							*it = new_substate_ptr;
						else {
							tp.state.erase(it);
						}
						break;
					}
				}
				// address is removed from the new right_substate
			//}
		}
		prune_and_normalize();
		return *this;
	}


	// Non member functions
	TensorEnsemble operator * (const TensorEnsemble& te, const double& alpha) {
		TensorEnsemble result = te;
		for (TensorPair& tp: result) {
			tp.probability *= alpha;
		}
		return result;
	}
	TensorEnsemble operator * (const double& alpha, const TensorEnsemble& te) {
		return te*alpha;
	}

	TensorState find_involved_sublocal_states(const QVariable& input, const TensorState& ts) {
		auto qvar_addresses = input.get_addresses();
		TensorState involved_sublocal_states;
		for(auto& sublocal_state_ptr: ts) {
			auto sublocal_addresses = sublocal_state_ptr->get_own_bit_addresses();
			for(short addr: sublocal_addresses) {
				if (contains(addr, qvar_addresses)) {
					involved_sublocal_states.push_back(sublocal_state_ptr);
					break;
				}
			}
		}
		return involved_sublocal_states;
	}

	TensorState magic_merge(TensorState& ts) {
		// TODO: rewrite this function using python preprocessor
		enum { sparse_state = 0, dense_state, density_matrix } state_type = sparse_state;
		for(auto ls_ptr: ts) {
			std::string class_type = ls_ptr->class_type();
			if (class_type == "LocalStateDenseState")
				state_type = MAX(state_type, dense_state);
			if (class_type == "LocalStateDensityMatrix")
				state_type = MAX(state_type, density_matrix);
		}
		if (state_type == sparse_state) {
			std::shared_ptr<LocalStateSparseState> output = 
				std::make_shared<LocalStateSparseState>(
					*std::dynamic_pointer_cast<LocalStateSparseState>(ts[0])
				);
			output->_get_data() = 
				std::dynamic_pointer_cast<LocalStateSparseState>(ts[0])->_get_data();
			for (int i=1; i<ts.size(); i++) {
				output = std::make_shared<LocalStateSparseState>(
					*output, 
					*std::dynamic_pointer_cast<LocalStateSparseState>(ts[i])
				);
			}
			return TensorState(1, std::static_pointer_cast<LocalState>(output));
		}
		else if (state_type == dense_state) {
			std::shared_ptr<LocalStateDenseState> output = 
				std::make_shared<LocalStateDenseState>(
					*std::dynamic_pointer_cast<LocalStateDenseState>(ts[0])
				);
			output->_get_data() = 
				std::dynamic_pointer_cast<LocalStateDenseState>(ts[0])->_get_data();
			for (int i=1; i<ts.size(); i++) {
				output = std::make_shared<LocalStateDenseState>(
					*output, 
					*std::dynamic_pointer_cast<LocalStateDenseState>(ts[i])
				);
			}
			return TensorState(1, std::static_pointer_cast<LocalState>(output));
		}
		else /* (state_type == density_matrix) */{
			std::shared_ptr<LocalStateDensityMatrix> output = 
				std::make_shared<LocalStateDensityMatrix>(
					*std::dynamic_pointer_cast<LocalStateDensityMatrix>(ts[0])
				);
			output->_get_data() = 
				std::dynamic_pointer_cast<LocalStateDensityMatrix>(ts[0])->_get_data();
			for (int i=1; i<ts.size(); i++) {
				output = std::make_shared<LocalStateDensityMatrix>(
					*output, 
					*std::dynamic_pointer_cast<LocalStateDensityMatrix>(ts[i])
				);
			}
			return TensorState(1, std::static_pointer_cast<LocalState>(output));
		}

	}

	std::ostream& operator << (std::ostream& os, LocalStateSparseState& ls) {
		os << "My type: " << ls.class_type() << std::endl;
		os << "My addresses: ";
		FOR(i, ls.get_own_bit_addresses().size()){
			os << " " << ls.get_own_bit_addresses()[i];
		}
		os << std::endl;
		os << "My data: "<< "(with size " <<ls._get_data().size()<< ")" << std::endl;
		for(SparseVectorXcf::InnerIterator it(ls._get_data()); it; ++it) {
			os << std::bitset<64>(it.index()) << it.value() << std::endl;
		}
		//os << std::endl;
		return os;
	}
	std::ostream& operator << (std::ostream& os, LocalStateDenseState& ls) {
		os << "My type: " << ls.class_type() << std::endl;
		os << "My addresses: ";
		FOR(i, ls.get_own_bit_addresses().size()){
			os << " " << ls.get_own_bit_addresses()[i];
		}
		os << std::endl;
		os << "My data: " << std::endl;
		FOR(i, ls._get_data().size()){
			os << " " << ls._get_data().coeffRef(i);
		}
		//os << std::endl;
		return os;
	}
	std::ostream& operator << (std::ostream& os, LocalStateTensorEnsemble& ls) {
		os << "My type: " << ls.class_type() << std::endl;
		os << "My addresses: ";
		FOR(i, ls.get_own_bit_addresses().size()){
			os << " " << ls.get_own_bit_addresses()[i];
		}
		os << std::endl;
		os << "My data things..............: " << std::endl;
		os << ls._get_data() <<std::endl;
		return os;
	}
	std::ostream& operator << (std::ostream& os, TensorEnsemble& te) {
		for(TensorPair tp: te) {
			os << "TensorPair with probability " << tp.probability 
			   << " and following states:" << std::endl;
			for(auto ls_ptr: tp.state) {
				if (ls_ptr->class_type() == "LocalStateSparseState")
					os << *std::dynamic_pointer_cast<LocalStateSparseState>(ls_ptr);
				if (ls_ptr->class_type() == "LocalStateDenseState")
					os << *std::dynamic_pointer_cast<LocalStateSparseState>(ls_ptr);
				if (ls_ptr->class_type() == "LocalStateTensorEnsemble")
					os << *std::dynamic_pointer_cast<LocalStateTensorEnsemble>(ls_ptr);
			}
			os << "End of following states." << std::endl;
		}
		return os;
	}


}
