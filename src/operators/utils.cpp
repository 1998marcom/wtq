#include "operators/utils.hpp"
#include <cmath>
//#include <chrono>
#include <iostream>

//using namespace std;
//using namespace std::chrono;

namespace wtq {
	Functor::Functor(const int& dimensions) {
		this->dimensions = dimensions;
	}
	SparseMatrixXcf Functor::to_hilbert_matrix() {
		ll N = ((ll) 1) << dimensions;
		SparseMatrixXcf result(N, N);
		FORL(i, N) {
			result.coeffRef((*this)(i), i) = cf(1,0);
		}
		return result;
	}

	MultiFunctor::MultiFunctor(const int& dimensions) {
		this->dimensions = dimensions;
	}
	SparseMatrixXcf MultiFunctor::to_hilbert_matrix(const ll& inverse_verbosity) {
		ll N = ((ll) 1) << dimensions;
		SparseMatrixXcf result(N, N);
		std::vector<Triplet<cf>> triplets;
		FORL(i, N) {
			if (inverse_verbosity>0 and i%inverse_verbosity == 0)
				std::cout << "Building MultiFunctor matrix: step " 
					   	  << i << "/" << N << std::endl;
			SparseVectorXcf v = (*this)(i);
			for(SparseVectorXcf::InnerIterator it(v); it; ++it) {
				triplets.push_back(Triplet<cf>(it.index(), i, it.value()));
			}
		}
		result.setFromTriplets(triplets.begin(), triplets.end());
		return result;
	}

	template<typename MatrixT>
	ControlU<MatrixT>::ControlU(const MatrixT& U) {
		cu.resize(U.rows()*2, U.cols()*2); cu.setZero();
		FORL(i, U.rows()) cu.coeffRef(i, i) = cf(1,0);
		FORL(k, U.outerSize()) {
			for(typename MatrixT::InnerIterator it(U, k); it; ++it) {
				cu.coeffRef(U.rows()+it.row(), U.cols()+it.col()) = it.value();
			}
		}
	}
	template <typename T>
	void ControlU<T>::operator() (const QBit& control, const QVariable& input, const int& direction) const {
		if (direction == 1) {
			QVariable(control, input) *= cu;
		}
		else if (direction == -1) {
			T adjcu = cu.adjoint();
			QVariable(control, input) *= adjcu;
		}
		else 
			throw std::runtime_error("Invalid ControlU direction");

	}
	template <typename T>
	const T ControlU<T>::get_matrix() const {
		return cu;
	}
	PYTHON_PREPROCESSOR{{{output(MCWS("""
	template class ControlU<MatrixType>;
	"""))}}}

	template<typename MatrixT>
	ControlH<MatrixT>::ControlH(const MatrixT& H) {
		ch.resize(H.rows()*2, H.cols()*2); ch.setZero();
		FORL(k, H.outerSize()) {
			for(typename MatrixT::InnerIterator it(H, k); it; ++it) {
				ch.coeffRef(H.rows()+it.row(), H.cols()+it.col()) = it.value();
			}
		}
	}
	template <typename T>
	void ControlH<T>::operator() (const QBit& control, const QVariable& input, const int& direction) const {
		if (direction == 1) {
			QVariable(control, input) *= ch;
		}
		else if (direction == -1) {
			T mH = -1.0*ch;
			QVariable(control, input) *= mH;
		}
		else 
			throw std::runtime_error("Invalid ControlH direction");

	}
	template <typename T>
	const T ControlH<T>::get_matrix() const {
		return ch;
	}
	PYTHON_PREPROCESSOR{{{output(MCWS("""
	template class ControlH<MatrixType>;
	"""))}}}

	template<typename MatrixT>
	StoneUnitary<MatrixT>::StoneUnitary(const MatrixT& herm, const bool& high_precision) {
		this->herm = herm;
		this->high_precision = high_precision;
	}
	template<typename MatrixT>
	const MatrixT StoneUnitary<MatrixT>::get_matrix() {
		if (this->unitary.cols() != this->herm.cols()) {
			auto lambda_max_est = estimate_lambda_max(herm);
			auto r_trotter = (int) ((20.0+high_precision*1500)*lambda_max_est);
			long long exp_taylor_order = (long long) 2.0*log10(lambda_max_est)+16;

			MatrixT result(herm.rows(), herm.cols());
			result.setZero();
			MatrixT tmp(herm.rows(), herm.cols());
			tmp.setIdentity();

			FOR(order, exp_taylor_order+1) {
				cf coeff;
				if (order < 20) {
					coeff = std::pow(
						cf(0,1)/((double)r_trotter), 
						order
					) / ((double) factorial(order));
				}
				else {
					coeff = std::pow(
						std::complex<double>(0,1)/((double)r_trotter), 
						order
					) / ((double) stirling_factorial((double) order));
				}
				result += coeff * tmp;
				tmp = tmp * herm;
			}

			tmp = result;
			result.setIdentity();
			FOR(_, r_trotter) {
				result = result * tmp;
			}
			this->unitary = result;
		}
		return this->unitary;
	}

	template<typename MatrixT>
	bool StoneUnitary<MatrixT>::is_get_matrix_convenient() const {
		if constexpr (std::is_same<MatrixT, MatrixXcf>::value) {
			return herm.cols() <= 16;
		}
		else {
			return herm.cols() <= int_pow(2, 11);
		}
	}

	template<typename MatrixT>
	const QVariable& operator*= (const QVariable& qvar, StoneUnitary<MatrixT>& su) {
		// TODO: Further optimization possible with low-level support of
		// \sum_i a_i \prod_j U_ij * data
		//Build matrix combination
		/*
		auto lambda_max_est = estimate_lambda_max(su.herm);
		auto r_trotter = (int) (17.0/2.0*lambda_max_est);
		int exp_taylor_order = (int) 2.0*log10(lambda_max_est)+13;

		std::vector<std::pair<cf, std::vector<MatrixT>>> mc;
		FOR(order, exp_taylor_order+1) {
			cf coeff = std::pow(
					cf(0,1)/((float)r_trotter), 
					order
				) / ((float) factorial(order));
			std::vector<MatrixT> mat_prod;
			FOR(_, order) {
				mat_prod.push_back(su.herm);
			}
			mc.push_back(std::pair(coeff, mat_prod));
		}
*/
		/*
		std::cout << "\tMultiplying qvar by stone unitary:\n" 
		          << "\t\testimated max eigenvalue: " << lambda_max_est << "\n" 
				  << "\t\torder of taylor expansion: " << exp_taylor_order << "\n" 
				  << "\t\ttrotterization steps: " << r_trotter << std::endl;
				  */
/*		FOR(_, r_trotter) {
			
			//std::cout << "C" << std::endl;
			qvar.apply_Us(mc);
		}*/
		//std::cout << "\tCUSU: multiplying done" << std::endl;
		if (su.is_get_matrix_convenient()) {
			qvar *= su.get_matrix();
		}
		else {
			qvar.apply_exp(su.herm);
		}
		return qvar;
	}
	PYTHON_PREPROCESSOR{{{output(MCWS("""
	template class StoneUnitary<MatrixType>;
	template const QVariable& operator*= (
		const QVariable& qvar, 
		StoneUnitary<MatrixType>& su
	);
	"""))}}}



}
