#include "operators/qft.hpp"
#include <cmath>
#include <numbers>
//#include <chrono>
//#include <iostream>

//using namespace std;
//using namespace std::chrono;

namespace wtq {

	MatrixXcf get_qft(const int& dimensions, const int& exponent_sign) {
		ll N = 1 << dimensions;
		MatrixXcf result(N, N);
		FOR(i, N) {
			FOR(j, N) {
				result.coeffRef(i, j) = 
					exp(exponent_sign*2*std::numbers::pi_v<double>*i*j/N*cf(0,1))
					/
					((double)sqrt((double) N));
			}
		}
		return result;
	}

	void qft(const QVariable& input, const int& exponent_sign) {
		//cout << "QFT start" << endl;
		//auto start = high_resolution_clock::now();
		auto qft_matrix = get_qft(input.get_addresses().size(), exponent_sign);
		//cout << "QFT Point 2, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		input *= qft_matrix;
		//cout << "QFT Point 3, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
	}

}
