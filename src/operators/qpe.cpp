#include "operators/qpe.hpp"
#include "operators/qft.hpp"
#include "operators/hadamard.hpp"
//#include <iostream>
#include "containers/localstate.hpp"
#include "containers/globalstate.hpp"
//#include <chrono>

//using namespace std;
//using namespace std::chrono;

namespace wtq {
	
	template <typename MatrixT>
	QPE<MatrixT>::QPE(const MatrixT& U, const int& estimation_bits) {
		this->U = U;
		auto my_U = U;
		FOR(i, estimation_bits) {
			cU2j.push_back(ControlU(my_U));
			//std::cout << "Adding U^(2^"<< i <<")" << std::endl;
			//std::cout << my_U << std::endl;
			//std::cout << "which becomes: " << std::endl;
			//std::cout << cU2j.back().get_matrix() << std::endl;
			my_U = my_U * my_U;
		}
	}
	template <typename MatrixT>
	void QPE<MatrixT>::operator() (const QVariable& phi, const QVariable& E, const int& direction) const {
		// E[0] is the most valuable digit
		//std::cout << *(phi._get_global_state_ptr());
		//auto start = high_resolution_clock::now();
		//cout << "QPE start" << endl;
		if (direction == 1)
			FOR(i, E.hilbert_dims()) {
				E[i] *= hadamard;
				//std::cout << *(phi._get_global_state_ptr());
			}
		//cout << "QPE Point 2, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		if (direction == -1)
			qft(E, 1);
		//cout << "QPE Point 3, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		FOR(i, cU2j.size()) {
			cU2j[cU2j.size()-i-1](E[i], phi, direction); 
			//std::cout << *(phi._get_global_state_ptr());
		}
		//cout << "QPE Point 4, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		//std::cout << *(phi._get_global_state_ptr());
		if (direction == 1)
			qft(E, -1);
		//cout << "QPE Point 5, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		if (direction == -1)
			FOR(i, E.hilbert_dims()) {
				MatrixXcf ha = hadamard.adjoint();
				E[i] *= ha;
				//std::cout << *(phi._get_global_state_ptr());
			}
		//cout << "QPE Point 6, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
	}

	PYTHON_PREPROCESSOR{{{output(MCWS("""
	template class QPE<MatrixType>;
	"""))}}}

	template <typename MatrixT>
	QPE_H<MatrixT>::QPE_H(const MatrixT& H) {
		this->cH = ControlH(H).get_matrix();
	}

	template <typename MatrixT>
	void QPE_H<MatrixT>::operator() (const QVariable& phi, const QVariable& E, const int& direction) {
		auto estimation_bits = E.hilbert_dims();
		// E[0] is the most valuable digit
		//std::cout << *(phi._get_global_state_ptr());
		//auto start = high_resolution_clock::now();
		//cout << "QPE start" << endl;
		if (direction == 1)
			FOR(i, E.hilbert_dims()) {
				E[i] *= hadamard;
				//std::cout << *(phi._get_global_state_ptr());
			}
		//cout << "QPE Point 2, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		if (direction == -1)
			qft(E, 1);
		//cout << "QPE Point 3, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		if (direction == 1) {
			if (positive_unitaries.size() == 0) {
				FOR(i, E.hilbert_dims()) {
					MatrixT hermitian = cH*cf(int_pow(2, E.hilbert_dims()-i-1),0);
					positive_unitaries.push_back(StoneUnitary(hermitian));
				}
			}
			FOR(i, E.hilbert_dims()) {
				QVariable(E[i], phi) *= positive_unitaries[i];
			}
		}
		if (direction == -1) {
			if (negative_unitaries.size() == 0) {
				FOR(i, E.hilbert_dims()) {
					MatrixT hermitian = cH*cf(-1*int_pow(2, E.hilbert_dims()-i-1),0);
					negative_unitaries.push_back(StoneUnitary(hermitian));
				}
			}
			FOR(i, E.hilbert_dims()) {
				QVariable(E[i], phi) *= negative_unitaries[i];
			}
		}
		//FOR(i, E.hilbert_dims()) {
			//MatrixT hermitian = cH*cf(direction*int_pow(2, E.hilbert_dims()-i-1),0);
			//QVariable(E[i], phi) *= StoneUnitary(hermitian);
			//std::cout << *(phi._get_global_state_ptr());
		//}
		//cout << "QPE Point 4, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		//std::cout << *(phi._get_global_state_ptr());
		if (direction == 1)
			qft(E, -1);
		//cout << "QPE Point 5, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;
		if (direction == -1)
			FOR(i, E.hilbert_dims()) {
				MatrixXcf ha = hadamard.adjoint();
				E[i] *= ha;
				//std::cout << *(phi._get_global_state_ptr());
			}
		//cout << "QPE Point 6, time from start [ms]: " << duration_cast<milliseconds>(high_resolution_clock::now()-start).count() << endl;

	}

	PYTHON_PREPROCESSOR{{{output(MCWS("""
	template class QPE_H<MatrixType>;
	"""))}}}

}
