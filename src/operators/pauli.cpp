#include "common.hpp"
#include "operators/pauli.hpp"
#include <stdexcept>

namespace wtq {
	MatrixXcf get_pauli(const Pauli& key) {
		MatrixXcf out(2,2);
		if (key == Pauli::Id) {
			out << cf(1.0,0.0), cf(0.0,0.0),
				   cf(0.0,0.0), cf(1.0,0.0);
		}
		else if (key == Pauli::X) {
			out << cf(0.0,0.0), cf(1.0,0.0),
				   cf(1.0,0.0), cf(0.0,0.0);
		}
		else if (key == Pauli::Y) {
			out << cf(0.0,0.0), cf(0.0,1.0),
				   cf(0.0,-1.0), cf(0.0,0.0);
		}
		else if (key == Pauli::Z) {
			out << cf(-1.0,0.0), cf(0.0,0.0),
				   cf(0.0,0.0), cf(1.0,0.0);
		}
		else {
			throw(std::runtime_error("Asked a pauli matrix not supported"));
		}
		return out;
	}
	/**
	template <> PauliMatrix<Pauli::Id>::PauliMatrix() {
		this->resize(2,2);
		*this << cf(1.0,0.0), cf(0.0,0.0),
				cf(0.0,0.0), cf(1.0,0.0);
	}
	PYTHON_PREPROCESSOR{{{output("\n".join([
	f"//template class PauliMatrix<Pauli::{X}>;" for X in ["Id", "X", "Y", "Z"]
	]))}}}
	*/
}
