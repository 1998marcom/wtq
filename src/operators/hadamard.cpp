#include "operators/hadamard.hpp"
#include <cmath>

namespace wtq {
	MatrixXcf get_hadamard() {
		MatrixXcf result(2,2);
		result << cf(1,0), cf(-1,0),
			      cf(1,0), cf(1,0);
		return result/((double) sqrt(2.0));
	}
}
