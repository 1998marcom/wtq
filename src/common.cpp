#include "common.hpp"
#include "containers/globalstate.hpp"
#include "operators/utils.hpp"
#include <stdexcept>
#include <iostream>

namespace wtq {

	short QBit::get_address() const {
		return bit_number;
	}
	double QBit::get_value() const {
		return global_state_ptr->get_value(bit_number);
	}
	int QBit::measure(const int& force_result) const {
		return this->global_state_ptr->measure(this->bit_number, force_result);
	}
	QBit::QBit(GlobalState* in_global_state_ptr, const short& in_bit_number) {
		this->global_state_ptr = in_global_state_ptr;
		this->bit_number = in_bit_number;
	}
	GlobalState* QBit::_get_global_state_ptr() const {
		return global_state_ptr;
	}
	template <typename T> const QBit& QBit::operator *= (const T& U) const {
		global_state_ptr->apply_U(*this, U);
		return *this;
	}
	template <typename T> const QBit& QBit::reset(const T& data) const {
		global_state_ptr->smart_reset(*this, data);
		return *this;
	}
	PYTHON_PREPROCESSOR{{{
	output(MCWS("""
	template const QBit& QBit::operator *= (const MatrixType& U) const;
	"""))
	output(MCWS("""
	template const QBit& QBit::reset(const DataType& U) const;
	"""))
	}}}
	StateType QBit::max_state_type() const {
		return this->global_state_ptr->max_state_type(*this);
	}

	//std::vector<short> QVariable::get_addresses() const {
	//	return std::vector(bit_numbers);
	//}
	const std::vector<short>& QVariable::get_addresses() const {
		return bit_numbers;
	}
	Eigen::VectorXf QVariable::get_values() const {
		Eigen::VectorXf result(bit_numbers.size());
		FOR(i, bit_numbers.size()) {
			double value = global_state_ptr->get_value(bit_numbers[i]);
			result[i] = value;
		}
		return result;
	}
	std::vector<int> QVariable::measure(const std::vector<int>& force_result) const {
		std::vector<int> result;
		if (force_result.size() == 0) {
			FOR(i, bit_numbers.size()) {
				result.push_back((*this)[i].measure());
			}
		}
		else {
			FOR(i, bit_numbers.size()) {
				result.push_back((*this)[i].measure(force_result[i]));
			}
		}
		return result;
	}
	ll QVariable::measure_int(const ll& force_result) const {
		auto force_result_vec = std::vector<int>();
		if (force_result >= 0) {
			force_result_vec.resize(hilbert_dims());
			ll local_force_result = force_result;
			FOR(i, hilbert_dims()) {
				force_result_vec[hilbert_dims()-1-i] = local_force_result & ((ll) 1);
				local_force_result = local_force_result >> 1;
			}
		}
		std::vector<int> e_v = measure(force_result_vec);
		ll e = 0;
		FOR(i, e_v.size()) {
			e += (((ll) 1) << i) * e_v[e_v.size()-1-i];
		}
		return e;
	}
	int QVariable::hilbert_dims() const {
		return bit_numbers.size();
	}
	ll QVariable::data_size() const {
		return ((ll) 1) << hilbert_dims();
	}
	QVariable::QVariable(
			GlobalState* in_global_state_ptr, 
			const std::vector<short>& bit_numbers) {
		this->global_state_ptr = in_global_state_ptr;
		this->bit_numbers = std::vector(bit_numbers);
	}
	//QVariable(const QBit& qbit);
	PYTHON_PREPROCESSOR{{{
		for i in range(10):
			outstr = "QVariable::QVariable(const QBit& qbit0"
			for j in range(1,i+1):
				outstr += f", const QBit& qbit{j}"
			output(outstr+") ")
			output("{")
			output("global_state_ptr = qbit0._get_global_state_ptr();")
			output("bit_numbers = std::vector<short>(1, qbit0.get_address());")
			for j in range(1, i+1):
				output(f"""
					if (global_state_ptr != qbit{j}._get_global_state_ptr())
						throw std::runtime_error("Cannot combine variables of different global states");
					bit_numbers.push_back(qbit{j}.get_address());
				""")
			output("}")
		for i in range(10):
			outstr = "QVariable::QVariable(const QVariable& qvar0"
			for j in range(1,i+1):
				outstr += f", const QVariable& qvar{j}"
			output(outstr+") ")
			output("{")
			output("global_state_ptr = qvar0._get_global_state_ptr();")
			output("bit_numbers = qvar0.get_addresses();")
			for j in range(1, i+1):
				output(f"""
					if (global_state_ptr != qvar{j}._get_global_state_ptr())
						throw std::runtime_error("Cannot combine variables of different global states");
					auto qvar{j}_addrs = qvar{j}.get_addresses();
					bit_numbers.insert(bit_numbers.end(),qvar{j}_addrs.begin(),qvar{j}_addrs.end());
				""")
			output("}")
	}}}
	GlobalState* QVariable::_get_global_state_ptr() const {
		return this->global_state_ptr;
	}
	QBit QVariable::operator [] (const int& index) const {
		return QBit(global_state_ptr, bit_numbers[index]);
	}
	template <typename T> const QVariable& QVariable::operator *= (const T& U) const {
		global_state_ptr->apply_U(*this, U);
		return *this;
	}
	template <typename T> const QVariable& QVariable::reset(const T& data) const {
		global_state_ptr->smart_reset(*this, data);
		return *this;
	}
	PYTHON_PREPROCESSOR{{{
	output(MCWS("""
	template const QVariable& QVariable::operator *= (const MatrixType& U) const;
	"""))
	output(MCWS("""
	template const QVariable& QVariable::reset(const DataType& U) const;
	"""))
	}}}
	StateType QVariable::max_state_type() const {
		StateType st = StateType::sparse_state;
		FOR(i, hilbert_dims()) {
			st = MAX(st, this->global_state_ptr->max_state_type((*this)[i]));
		}
		return st;
	}
	PYTHON_PREPROCESSOR{{{output(MCWS("""
	const QVariable& QVariable::apply_exp(const MatrixType& H) const {
		this->global_state_ptr->apply_exp(*this, H);
		return *this;
	}
	const QVariable& QVariable::apply_Us(const MatrixTypeCombination& mc) const {
		this->global_state_ptr->apply_Us(*this, mc);
		return *this;
	}
	"""))}}}

	class SwapFunctor : public Functor {
		using Functor::Functor;
		virtual ll operator() (const ll& in) {
			/** We turn an integer written in binary as AB into BA
			 *  ex.: 001-110 --> 110-001 
			 *  Note: the middle dash is just for clarity
			 */
			ll right_bitmask = (((ll) 1) << dimensions/2) - 1;
			ll left_bitmask = (((ll) 1) << dimensions) - 1 - right_bitmask;
			ll left_part = (in & right_bitmask) << dimensions/2;
			ll right_part = (in & left_bitmask) >> dimensions/2;
			return left_part + right_part;
		}
	};
	void swap(const QVariable& a, const QVariable& b) {
		if (a.hilbert_dims() != b.hilbert_dims())
			throw std::runtime_error("Cannot swap variables with different lengths");
		auto swap_functor = SwapFunctor(a.hilbert_dims()+b.hilbert_dims());
		SparseMatrixXcf swapmatrix = swap_functor.to_hilbert_matrix();
		QVariable(a, b) *= swapmatrix;
		//TODO optimize with final factorization
	}
	
	template<typename MatrixT>
	double estimate_lambda_max(const MatrixT& mat) {
		VectorXcf V; 
		V.resize(mat.cols());
		V.setRandom();
		FOR(_, 100) {
			V = mat * V;
			V = V / V.norm();
		}
		return (mat*V).norm() / V.norm();
	}

	PYTHON_PREPROCESSOR{{{
	output(MCWS("""
	template double estimate_lambda_max(const MatrixType& mat);
	"""))
	}}}



}
