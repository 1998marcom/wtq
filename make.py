#!/usr/bin/env python3
import argparse, tempfile, shutil
import subprocess as s
from pathlib import Path
import re

parser = argparse.ArgumentParser(description="Build script for library and tests")
parser.add_argument("-t", "--tests", action="store_true", default=False)
parser.add_argument("-T", "--test", action="store", default=None)
parser.add_argument("-C", "--nocompile", action="store_true", default=False)
parser.add_argument("-P", "--nopreprocess", action="store_true", default=False)
parser.add_argument("-I", "--noinstall", action="store_true", default=False)
parser.add_argument("-g", "--gpu", action="store_true", default=False)
args = parser.parse_args()

MAIN_DIR = Path(__name__).resolve().parent
BUILD_DIR = (MAIN_DIR/"build")

PREPROCESSOR_READINGS = [ 'preprocessor_code' ]
PREPROCESSOR = MAIN_DIR / 'preprocessor/main.py'
LIBRARY_SO = BUILD_DIR / "libwtq.so"
if not args.nopreprocess:
    BUILD_DIR.mkdir(exist_ok=True)
    print("Preprocessing includes...")
    s.run([
        str(PREPROCESSOR),
        '-r', *PREPROCESSOR_READINGS,
        '-i', 'include',
        '-o', str(BUILD_DIR),
    ])
    print("Preprocessing srcs...")
    s.run([
        str(PREPROCESSOR),
        '-r', *PREPROCESSOR_READINGS,
        '-i', 'src',
        '-o', str(BUILD_DIR),
    ])
if not args.nocompile:
    print("Cat-ting all includes into a single file")
    include_texts = {}
    include_order = []
    for file in (BUILD_DIR/"include").glob("**/*.hpp"):
        my_include_string = str(file.relative_to(BUILD_DIR/"include"))
        my_include_text = open(file, "r").read()
        my_includes = list(map(
            lambda x: x[10:-1], 
            re.findall('#include "[\w/\.]+"', my_include_text)
        ))
        my_include_text, n_subs = re.subn('#include "[\w/\.]+"\n', '', my_include_text)
        include_texts[my_include_string] = my_include_text
        # now we want to find the last include_string in include_order that is
        # in my_includes, so that we can place our include_string exactly after
        # that last include_string we found
        last_include_string_pos = 0
        for i, include_string in enumerate(include_order):
            if include_string in my_includes:
                last_include_string_pos = i
        include_order.insert(last_include_string_pos+1, my_include_string)
    with open(BUILD_DIR/"wtq", "w") as header_file:
        header_file.write(
"""
#ifndef WTQ_HEADER
#define WTQ_HEADER
"""
        )
        for include_string in include_order:
            header_file.write(include_texts[include_string])
        header_file.write(
"""
#endif
"""
        )
    print("Compiling shared library...")
    try:
        LIBRARY_SO.unlink()
    except:
        pass
    s.run([
        'hipcc' if args.gpu else 'g++',
        *list(map(str,(BUILD_DIR/"src").glob("**/*.cpp"))),
        '-I'+str(BUILD_DIR/"include"),
        '-std=gnu++20',
        '-fPIC',
        '-shared',
        '-O3',
        '-o', str(LIBRARY_SO),
    ])
    if LIBRARY_SO.exists():
        print("Compilation succeded")
    else:
        exit("Library compilation failed")

if args.tests or args.test:
    TESTS_DIR = MAIN_DIR / 'test'
    TESTS_BUILD_DIR = BUILD_DIR / 'test'
    TESTS_BUILD_DIR.mkdir(exist_ok=True, parents=True)
    for test_dir in sorted(list(TESTS_DIR.iterdir())):
        if not test_dir.is_dir(): continue
        if not args.tests and test_dir.name != args.test: 
            continue
        print(f"Working on test: {test_dir.name}")
        test_build_dir = TESTS_BUILD_DIR / test_dir.name
        test_build_dir.mkdir(exist_ok=True)
        exe_path = test_build_dir/"testcase"
        print("\tCompiling testcase...")
        s.run([
            'hipcc' if args.gpu else 'g++',
            *list(map(str,test_dir.glob("**/*.cpp"))),
            '-I'+str(BUILD_DIR/"include"),
            '-I'+str(BUILD_DIR),
            '-L'+str(BUILD_DIR),
            '-l'+"wtq",
            '-O3',
            '-std=gnu++20',
            '-o', str(exe_path),
        ])
        if exe_path.exists():
            print("\tRunning testcase...")
            s.run(exe_path)
            print("\tTest done. Did you see any error?")
        else:
            print("\tWhoops, compilation seems to have failed")

if not args.noinstall:
    
    include_install_dir = Path('/usr/include/')
    library_install_dir = Path('/usr/lib/x86_64-linux-gnu')

    include_input_dir = input(
        "I am installing the library headers in your system.\n" +
       f"Where do you want them to be installed? [default: {include_install_dir}]\n"
    )
    if include_input_dir: include_install_dir = Path(include_input_dir)
    
    shutil.copy(BUILD_DIR/"wtq", include_install_dir/"wtq")
    print("OK! Headers installed")

    library_input_dir = input(
        "I am installing the compiled library binaries in your system.\n" +
       f"Where do you want them to be installed? [default: {library_install_dir}]\n"
    )
    if library_input_dir: library_install_dir = Path(library_input_dir)

    shutil.copy(BUILD_DIR/"libwtq.so", library_install_dir/"libwtq.so")
    print("OK! Library binaries installed")
