<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Wider Than Quantum](#wider-than-quantum)
    - [Hello world](#hello-world)
    - [Docs](#docs)
    - [Internals](#internals)
    - [Compiling](#compiling)
        - [Dependencies](#dependencies)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Wider Than Quantum
---
This is the repository for Wider Than Quantum (shortened as `wtq`), a c++
library that eases simulation of mixed quantum-classical circuits.

### Hello world
The first test case is a valid "hello world" 
that shows how Wider Than Quantum works:
```cpp
[includes]

int main() {

    // GlobalState instantiation
    wtq::GlobalState global_state;

    // Variable instantiation (might be entangled)
    wtq::QVariable A = global_state.add_variable(2); // Add a variable with 2 qubits
    wtq::QVariable B = global_state.add_variable(8); // Add a variable with 8 qubits
    wtq::QVariable C = global_state.add_variable(40); // Add a variable with 40 qubits

    // Variable initialization
    wtq::SparseVectorXcf A_initial_state(4);
    A_initial_state.coeffRef(2) = complex<float>(1.0/sqrt(2.0),0.0); // <10|A>
    A_initial_state.coeffRef(3) = complex<float>(1.0/sqrt(2.0),0.0); // <11|A>
    A.reset(A_initial_state);

    wtq::SparseVectorXcf B_initial_state(wtq::int_pow(2, 8));
    B_initial_state.coeffRef(0) = complex<float>(1.0, 0.0);
    B.reset(B_initial_state);

    // Operation U between A[1] and B[0]
    wtq::QVariable a1b0 = wtq::QVariable(A[1], B[0]); // Order of QBits matters
    wtq::MatrixXcf U(4,4);
    U << 1, 0, 0, 0,
         0, 1, 0, 0,
         0, 0, 0, 1,
         0, 0, 1, 0; // C-NOT 
    a1b0 *= U; // Note that the whole system is affected by the U multiplication
               // and a copy-on-write or returning a qvariable of another system
               // would be tremendously resource-hungry. As such, and to remind
               // you of the way quantum systems work, only the *= operator is
               // available

    // Equivalent code using built-in pauli matrixes and c-u
    wtq::ControlU c_not = wtq::ControlU(wtq::PauliX); // C-NOT
    c_not(A[1], B[0]);

    // QBits are often implicitly casted to QVariables, so the previous line
    // really means:
    c_not(A[1], wtq::QVariable(B[0])); 

    // For debug purposes, you can cout the global_state
    //cout << global_state << endl;

    // Measuring Qbits
    vector<int> A_measure = A.measure();
    int B0_measure = B[0].measure();

    cout << "Measured A: " << A_measure[0] << " " << A_measure[1] << endl;
    cout << "Measured B[0]: " << B0_measure << endl;

    return 0;
}
```

### Docs
The code is objectively extremely poorly documented. The doxygen auto-generated
documentation is available at 
[1998marcom.gitlab.io/wtq](https://1998marcom.gitlab.io/wtq).


### Internals
It is built on top of Eigen (which should allow for a vendor-neutral GPU
implementation through HIP). Still, the backend used is important only when
dealing with raw data.

### Compiling
All sources are preprocessed through a custom preprocessor
(`preprocessor/main.py`). This custom preprocessor interprets as python code
everything included inside the `PYTHON_PREPROCESSOR{{{`-python code-`}}}` tag
scheme.

The whole compilation workflow is given by the script `make.py`. 
To also run the tests, run `./make.py -t`. In case of doubts, `./make.py -h`.
The script recursively preprocess each file in the `src` and `include` dirs, and
compiles the code using `g++` (using C++20). The documentation is generated
from preprocessed sources.

##### Dependencies
Dependencies for compiling code: eigen, python3, g++. On debian-based
distributions:
```
apt install libeigen3-dev python3 g++
```
Make sure the installed version of g++ has reasonable support for c++20 (e.g.
g++ >= 10)

Dependencies for compiling documentation: doxygen, graphvix, 
the 'Liberation Sans' font. On debian-based distributions:
```
apt install doxygen, graphviz, fonts-liberation2
```

